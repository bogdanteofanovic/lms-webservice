package rs.singi.dev.restapp.service.university.enrollment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.university.enrollment.EnrolledYearOfStudy;
import rs.singi.dev.restapp.model.university.enrollment.EnrollmentYearType;
import rs.singi.dev.restapp.repository.universtiy.enrollment.EnrollmentYearTypeRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class EnrollmentYearTypeService extends GenericService<EnrollmentYearType, EnrollmentYearTypeRepository> {
}