package rs.singi.dev.restapp.service.university.faculty.classroom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.university.enrollment.EnrollmentYearType;
import rs.singi.dev.restapp.model.university.faculty.Classroom;
import rs.singi.dev.restapp.repository.universtiy.faculty.ClassroomRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class ClassroomService extends GenericService<Classroom, ClassroomRepository> {
}