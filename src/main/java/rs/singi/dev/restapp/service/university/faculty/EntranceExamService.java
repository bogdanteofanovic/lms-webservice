package rs.singi.dev.restapp.service.university.faculty;

import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.university.faculty.EntranceExam;
import rs.singi.dev.restapp.repository.universtiy.faculty.EntranceExamRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class EntranceExamService extends GenericService<EntranceExam, EntranceExamRepository> {
}
