package rs.singi.dev.restapp.service.subject.realisation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.subject.realisation.ScheduleRealisation;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationMaterial;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.repository.subject.realisation.SubjectRealisationMaterialRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class SubjectRealisationMaterialService extends GenericService<SubjectRealisationMaterial, SubjectRealisationMaterialRepository> {
    public Iterable<SubjectRealisationMaterial> findBySubjectRealisationIdAndDeletedIsFalse(Long id){
        return repository.findBySubjectRealisationIdAndDeletedIsFalse(id);
    }

}
