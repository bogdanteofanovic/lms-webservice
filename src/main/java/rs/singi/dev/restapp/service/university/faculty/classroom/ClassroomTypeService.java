package rs.singi.dev.restapp.service.university.faculty.classroom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.university.faculty.Classroom;
import rs.singi.dev.restapp.model.university.faculty.ClassroomType;
import rs.singi.dev.restapp.repository.universtiy.faculty.ClassroomTypeRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class ClassroomTypeService extends GenericService<ClassroomType, ClassroomTypeRepository> {
}