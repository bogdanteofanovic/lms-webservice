package rs.singi.dev.restapp.service.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.employee.Employee;
import rs.singi.dev.restapp.repository.employee.EmployeeTypeRepository;
import rs.singi.dev.restapp.model.employee.EmployeeType;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class EmployeeTypeService extends GenericService<EmployeeType, EmployeeTypeRepository> {
}
