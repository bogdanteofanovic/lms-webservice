package rs.singi.dev.restapp.service.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.file.DBFile;
import rs.singi.dev.restapp.model.file.FSFile;
import rs.singi.dev.restapp.repository.file.DBFileRepository;

import java.util.Optional;

@Service
public class DBFileService extends GenericFileService<DBFile, DBFileRepository> {

    public Iterable<DBFile> getByFileName(String fileName) {
        return repository.getByFileName(fileName);
    }
    public Iterable<String[]> getAllFileNames() {
        return repository.getAllFileNames();
    }
    public Iterable<DBFile> findAll() {
        return repository.findByDeletedIsFalse();
    }
    public Iterable<DBFile> findAllDeleted() {
        return repository.findByDeletedIsTrue();
    }
}
