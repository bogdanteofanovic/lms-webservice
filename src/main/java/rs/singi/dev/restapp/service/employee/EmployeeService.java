package rs.singi.dev.restapp.service.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.address.Country;
import rs.singi.dev.restapp.model.employee.Employee;
import rs.singi.dev.restapp.repository.employee.EmployeeRepository;
import rs.singi.dev.restapp.service.GenericService;


@Service
public class EmployeeService extends GenericService<Employee, EmployeeRepository> {
}
