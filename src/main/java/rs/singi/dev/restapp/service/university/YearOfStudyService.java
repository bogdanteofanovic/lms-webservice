package rs.singi.dev.restapp.service.university;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.university.University;
import rs.singi.dev.restapp.model.university.YearOfStudy;
import rs.singi.dev.restapp.repository.universtiy.YearOfStudyRepository;
import rs.singi.dev.restapp.service.GenericService;


@Service
public class YearOfStudyService extends GenericService<YearOfStudy, YearOfStudyRepository> {
}
