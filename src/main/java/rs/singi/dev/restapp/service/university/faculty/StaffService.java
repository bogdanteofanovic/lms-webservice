package rs.singi.dev.restapp.service.university.faculty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.university.faculty.Faculty;
import rs.singi.dev.restapp.model.university.faculty.Staff;
import rs.singi.dev.restapp.repository.universtiy.faculty.StaffRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class StaffService extends GenericService<Staff, StaffRepository> {
}