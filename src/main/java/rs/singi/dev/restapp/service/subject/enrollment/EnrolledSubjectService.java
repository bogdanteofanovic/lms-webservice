package rs.singi.dev.restapp.service.subject.enrollment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.employee.EmployeeType;
import rs.singi.dev.restapp.model.subject.enrollment.EnrolledSubject;
import rs.singi.dev.restapp.model.university.Student;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.repository.subject.enrollment.EnrolledSubjectRepository;
import rs.singi.dev.restapp.repository.universtiy.StudentRepository;
import rs.singi.dev.restapp.repository.user.UserRepository;
import rs.singi.dev.restapp.service.GenericService;

import java.util.Optional;

@Service
public class EnrolledSubjectService extends GenericService<EnrolledSubject, EnrolledSubjectRepository> {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private UserRepository userRepository;


    public Iterable<EnrolledSubject> findEnrolledSubjectsByStudentId(Long id) {
        return repository.findEnrolledSubjectsWhereStudentId(id);
    }

    public Iterable<EnrolledSubject> findByUsername(String username){

        //todo: ovo mozemo resiti tako sto se iz konteksta izvadi ulogovani korisnik i dobave se njegovi upisani predmeti

        Optional<User> user = userRepository.getByUsername(username);
        if(user.isPresent()){
            Optional<Student> student = studentRepository.findByUser(user.get());
            if(student.isPresent()){
                return repository.findEnrolledSubjectsWhereStudentId(student.get().getId());
            }
        }
        return null;
    }
}
