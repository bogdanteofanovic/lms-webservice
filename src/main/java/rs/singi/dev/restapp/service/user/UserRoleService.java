package rs.singi.dev.restapp.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.user.Role;
import rs.singi.dev.restapp.model.user.UserRole;
import rs.singi.dev.restapp.repository.user.UserRoleRepository;
import rs.singi.dev.restapp.service.GenericService;

import java.util.Set;

@Service
public class UserRoleService extends GenericService<UserRole, UserRoleRepository> {

    public Set<UserRole> getRolesByUserId(Long id) {
        return repository.getByUserId(id);
    }
}