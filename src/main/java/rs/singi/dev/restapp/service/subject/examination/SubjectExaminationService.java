package rs.singi.dev.restapp.service.subject.examination;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.subject.examination.SubjectExamination;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationMaterial;
import rs.singi.dev.restapp.repository.subject.examination.SubjectExaminationRepository;
import rs.singi.dev.restapp.service.GenericService;
@Service
public class SubjectExaminationService extends GenericService<SubjectExamination, SubjectExaminationRepository> {
}
