package rs.singi.dev.restapp.service.subject;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.subject.Subject;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationType;
import rs.singi.dev.restapp.repository.subject.SubjectRepository;
import rs.singi.dev.restapp.service.GenericService;


@Service
public class SubjectService extends GenericService<Subject, SubjectRepository> {
}
