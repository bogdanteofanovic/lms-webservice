package rs.singi.dev.restapp.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.model.user.UserRole;
import rs.singi.dev.restapp.repository.user.UserRepository;
import rs.singi.dev.restapp.service.GenericService;

import java.util.Optional;

@Service
public class UserService extends GenericService<User, UserRepository> {

    public Optional<User> getByUsername(String username) {
        return repository.getByUsername(username);
    }

    public Optional<User> getByUsernameAndPassword(String username, String password) {
        return repository.getByUsernameAndPassword(username, password);
    }

    public Optional<User> getByEmail(String email) {
        return repository.getByEmail(email);
    }


    public Iterable<User> findFreeUsers(){
        return repository.findUsersByEmployeeIsNullAndStudentIsNull();
    }

    public Optional<User> findByPublicId(String publicId){
        return repository.findByPublicId(publicId);
    }


}