package rs.singi.dev.restapp.service.subject.enrollment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.subject.enrollment.EnrolledSubject;
import rs.singi.dev.restapp.model.subject.enrollment.EnrollmentType;
import rs.singi.dev.restapp.repository.subject.enrollment.EnrollmentTypeRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class EnrollmentTypeService extends GenericService<EnrollmentType, EnrollmentTypeRepository> {
}
