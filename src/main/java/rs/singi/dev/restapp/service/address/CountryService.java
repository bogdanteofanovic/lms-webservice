package rs.singi.dev.restapp.service.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.address.Address;
import rs.singi.dev.restapp.model.address.Country;
import rs.singi.dev.restapp.repository.address.CountryRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class CountryService extends GenericService<Country, CountryRepository> {
}
