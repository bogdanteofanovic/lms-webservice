package rs.singi.dev.restapp.service.subject.realisation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisation;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationNotification;
import rs.singi.dev.restapp.repository.subject.realisation.SubjectRealisationRepository;
import rs.singi.dev.restapp.service.GenericService;


@Service
public class SubjectRealisationService extends GenericService<SubjectRealisation, SubjectRealisationRepository> {
}
