package rs.singi.dev.restapp.service.subject.examination;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationDetails;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationMaterial;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationMaterial;
import rs.singi.dev.restapp.repository.subject.examination.SubjectExaminationMaterialRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class SubjectExaminationMaterialService extends GenericService<SubjectExaminationMaterial, SubjectExaminationMaterialRepository> {
    public Iterable<SubjectExaminationMaterial> findBySubjectExaminationDetailsIdAndDeletedIsFalse(Long id){
        return repository.findBySubjectExaminationDetailsIdAndDeletedIsFalse(id);
    }

}
