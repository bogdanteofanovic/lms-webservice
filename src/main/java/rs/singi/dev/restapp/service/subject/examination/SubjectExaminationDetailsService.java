package rs.singi.dev.restapp.service.subject.examination;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.subject.examination.ScheduleExamination;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationDetails;
import rs.singi.dev.restapp.repository.subject.examination.SubjectExaminationDetailsRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class SubjectExaminationDetailsService extends GenericService<SubjectExaminationDetails, SubjectExaminationDetailsRepository> {
}
