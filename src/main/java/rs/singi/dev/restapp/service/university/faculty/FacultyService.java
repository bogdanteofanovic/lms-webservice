package rs.singi.dev.restapp.service.university.faculty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.university.faculty.ClassroomType;
import rs.singi.dev.restapp.model.university.faculty.Faculty;
import rs.singi.dev.restapp.repository.universtiy.faculty.FacultyRepository;
import rs.singi.dev.restapp.service.GenericService;


@Service
public class FacultyService extends GenericService<Faculty, FacultyRepository> {
}
