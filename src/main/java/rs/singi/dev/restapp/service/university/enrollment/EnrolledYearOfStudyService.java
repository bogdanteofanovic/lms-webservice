package rs.singi.dev.restapp.service.university.enrollment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.subject.Subject;
import rs.singi.dev.restapp.model.university.enrollment.EnrolledYearOfStudy;
import rs.singi.dev.restapp.repository.universtiy.enrollment.EnrolledYearOfStudyRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class EnrolledYearOfStudyService extends GenericService<EnrolledYearOfStudy, EnrolledYearOfStudyRepository> {
}