package rs.singi.dev.restapp.service.university;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.university.AcademicYear;
import rs.singi.dev.restapp.model.university.faculty.StudyProgram;
import rs.singi.dev.restapp.repository.universtiy.AcademicYearRepository;
import rs.singi.dev.restapp.service.GenericService;


@Service
public class AcademicYearService extends GenericService<AcademicYear, AcademicYearRepository> {
}
