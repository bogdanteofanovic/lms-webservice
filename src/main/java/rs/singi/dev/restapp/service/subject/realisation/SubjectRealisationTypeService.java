package rs.singi.dev.restapp.service.subject.realisation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisation;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationType;
import rs.singi.dev.restapp.repository.subject.realisation.SubjectRealisationTypeRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class SubjectRealisationTypeService extends GenericService<SubjectRealisationType, SubjectRealisationTypeRepository> {
}
