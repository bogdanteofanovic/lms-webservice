package rs.singi.dev.restapp.service.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.address.Address;
import rs.singi.dev.restapp.model.address.City;
import rs.singi.dev.restapp.model.file.DBFile;
import rs.singi.dev.restapp.repository.address.AddressRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class AddressService extends GenericService<Address, AddressRepository> {
}
