package rs.singi.dev.restapp.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.university.YearOfStudy;
import rs.singi.dev.restapp.model.user.Role;
import rs.singi.dev.restapp.repository.user.RoleRepository;
import rs.singi.dev.restapp.service.GenericService;

import java.util.Optional;

@Service
public class RoleService extends GenericService<Role, RoleRepository>{
    
    public Optional<Role> getByName(String name) {
        return repository.getByName(name);
    }
}
