package rs.singi.dev.restapp.service.subject.realisation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.subject.realisation.ProfessorOnRealisation;
import rs.singi.dev.restapp.model.subject.realisation.ScheduleRealisation;
import rs.singi.dev.restapp.repository.subject.realisation.ScheduleRealisationRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class ScheduleRealisationService extends GenericService<ScheduleRealisation, ScheduleRealisationRepository> {
}