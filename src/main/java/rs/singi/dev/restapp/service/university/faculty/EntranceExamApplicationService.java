package rs.singi.dev.restapp.service.university.faculty;

import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.university.faculty.EntranceExamApplication;
import rs.singi.dev.restapp.repository.universtiy.faculty.EntranceExamApplicationRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class EntranceExamApplicationService extends GenericService<EntranceExamApplication, EntranceExamApplicationRepository> {
}
