package rs.singi.dev.restapp.service.subject.examination;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.singi.dev.restapp.model.subject.examination.SubjectExamination;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationType;
import rs.singi.dev.restapp.repository.subject.examination.SubjectExaminationTypeRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class SubjectExaminationTypeService extends GenericService<SubjectExaminationType, SubjectExaminationTypeRepository> {
}
