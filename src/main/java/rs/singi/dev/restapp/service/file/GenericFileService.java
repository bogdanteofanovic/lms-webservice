package rs.singi.dev.restapp.service.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import rs.singi.dev.restapp.model.file.AbstractFile;

public abstract class GenericFileService<T extends AbstractFile, V extends PagingAndSortingRepository<T, Long>> {

    @Autowired
    protected V repository;

    public GenericFileService() {
    }

    public GenericFileService(V repository){
        this.repository = repository;
    }

    public T findOne(Long id) {
        return repository.findById(id).orElse(null);
    }

    public T storeFile(T entity) {

        return repository.save(entity);

    }

    public void deleteFile(Long id) {
        repository.deleteById(id);
    }

    public void deleteFile(T entity) {
        repository.delete(entity);
    }
}
