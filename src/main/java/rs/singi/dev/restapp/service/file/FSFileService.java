package rs.singi.dev.restapp.service.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import rs.singi.dev.restapp.model.file.DBFile;
import rs.singi.dev.restapp.model.file.FSFile;
import rs.singi.dev.restapp.repository.file.FSFileRepository;

@Service
public class FSFileService extends GenericFileService<FSFile, FSFileRepository> {

    private final Path root = Paths.get("uploads");

    public Iterable<String[]> getAllFileNames() {
        return repository.getAllFileNames();
    }

    public Iterable<FSFile> findAll() {
        return repository.findAllByDeletedFalse();
    }

    public Iterable<FSFile> findAllDeleted() {
        return repository.findAllByDeletedTrue();
    }

    public Iterable<FSFile> findByUserUsername(String username) {
        return repository.findByUserUsername(username);
    }

//    public Iterable<FSFile> findAllForSubjectRealisation(Long id) {
//        return repository.findAllForSubjectRealisation(id);
//    }
//
//    public Iterable<FSFile> findAllForSubjectExamination(Long id) {
//        return repository.findAllForSubjectExamination(id);
//    }

    public Optional<FSFile> getByFilePath(String filePath) {
        return repository.getByFilePath(filePath);
    }

    public Resource loadContent(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }
}
