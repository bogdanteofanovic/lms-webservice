package rs.singi.dev.restapp.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.repository.CustomRepository;

public abstract class GenericService<T extends AbstractEntity, V extends PagingAndSortingRepository<T, Long> & CustomRepository<T>> {

    @Autowired
    protected V repository;

    public GenericService() {
    }

    public GenericService(V repository){
        this.repository = repository;
    }

    public Iterable<T> findAll() {
        return repository.findAll();
    }

    public Iterable<T> findAllDeleted() {
        return repository.findAllByDeletedTrue();
    }

    public Iterable<T> findAllNotDeleted(){
        return repository.findAllByDeletedFalse();
    }


    public T findOne(Long id) {
        return repository.findById(id).orElse(null);
    }

    public T save(T entity) {

        return repository.save(entity);

    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    public void delete(T entity) {
        repository.delete(entity);
    }


}
