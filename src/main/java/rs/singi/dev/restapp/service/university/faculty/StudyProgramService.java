package rs.singi.dev.restapp.service.university.faculty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.singi.dev.restapp.model.university.faculty.StaffType;
import rs.singi.dev.restapp.model.university.faculty.StudyProgram;
import rs.singi.dev.restapp.repository.universtiy.faculty.StudyProgramRepository;
import rs.singi.dev.restapp.service.GenericService;

@Service
public class StudyProgramService extends GenericService<StudyProgram, StudyProgramRepository> {
}
