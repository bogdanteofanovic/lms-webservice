package rs.singi.dev.restapp.dto;

import rs.singi.dev.restapp.model.university.faculty.EntranceExam;

public class EntranceExamApplicationDto {

    private EntranceExam entranceExam;

    public EntranceExamApplicationDto(EntranceExam entranceExam) {
        this.entranceExam = entranceExam;
    }

    public EntranceExam getEntranceExam() {
        return entranceExam;
    }

    public void setEntranceExam(EntranceExam entranceExam) {
        this.entranceExam = entranceExam;
    }
}
