package rs.singi.dev.restapp.dto;

import rs.singi.dev.restapp.model.user.User;

public class UserDto {
    private String username;
    private String email;

    public UserDto() {
        super();
    }

    public UserDto(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public UserDto(User user) {
        this(user.getUsername(), user.getEmail());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
