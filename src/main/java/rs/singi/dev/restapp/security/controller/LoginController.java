package rs.singi.dev.restapp.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.singi.dev.restapp.dto.UserDto;
import rs.singi.dev.restapp.model.user.Role;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.model.user.UserRole;
import rs.singi.dev.restapp.security.model.AuthRequest;
import rs.singi.dev.restapp.service.user.UserRoleService;
import rs.singi.dev.restapp.service.user.UserService;
import rs.singi.dev.restapp.security.utils.JwtUtil;
import rs.singi.dev.restapp.service.user.RoleService;
import rs.singi.dev.restapp.utils.mail.MailUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Value("${activation.url}")
    private String activationUrl;

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtUtil tokenUtils;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MailUtils mailUtils;

    @CrossOrigin
    @RequestMapping(path = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, String>> login(@RequestBody AuthRequest user) {
        try {
            Optional<User> u = userService.getByUsername(user.getUsername());
            if(u.isPresent()){
                if(u.get().getActive()){

                    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getUsername(),
                            user.getPassword());

                    Authentication authentication = authenticationManager.authenticate(token);
                    SecurityContextHolder.getContext().setAuthentication(authentication);

                    UserDetails details = userDetailsService.loadUserByUsername(user.getUsername());
                    String userToken = tokenUtils.generateToken(details);

                    HashMap<String, String> data = new HashMap<String, String>();
                    data.put("token", userToken);

                    return new ResponseEntity<HashMap<String, String>>(data, HttpStatus.OK);

                }
            }


        } catch (Exception e) {
            return new ResponseEntity<HashMap<String, String>>(HttpStatus.UNAUTHORIZED);
        }

        return new ResponseEntity<HashMap<String, String>>(HttpStatus.LOCKED);
    }

    @CrossOrigin
    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<UserDto> register(@RequestBody User entity) {
        System.out.println(entity);
        if (userService.getByUsername(entity.getUsername()).isPresent() || userService.getByEmail(entity.getEmail()).isPresent()) {
            return new ResponseEntity<UserDto>(HttpStatus.CONFLICT);
        }
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));

        entity = userService.save(entity);
        entity.setUserRoles(new HashSet<UserRole>());
        Role userRole = roleService.getByName("ROLE_USER").orElse(null);
        UserRole newUserRole = new UserRole(entity, userRole);
        userRoleService.save(newUserRole);
        entity.getUserRoles().add(new UserRole(entity, userRole));
        User user  = userService.save(entity);

        mailUtils.sendEmail(user.getEmail(), "Activate user account", activationUrl + "/" + user.getPublicId());
        return new ResponseEntity<UserDto>(new UserDto(entity), HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserDto> edit(@PathVariable("id") Long id, @RequestBody User entity) {
        User user = userService.findOne(id);
        if (user == null) {
            return new ResponseEntity<UserDto>(HttpStatus.NOT_FOUND);
        }
        if (userService.getByEmail(entity.getEmail()).isPresent()) {
            return new ResponseEntity<UserDto>(HttpStatus.CONFLICT);
        }
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));

        user.setPassword(entity.getPassword());
        user.setEmail(entity.getEmail());
        user.setPassword(entity.getPassword());
        userService.save(user);
        return new ResponseEntity<UserDto>(new UserDto(user), HttpStatus.OK);
    }

    @RequestMapping("/test1")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> testAdmin() {
        return new ResponseEntity<String>("Hello, admin user!", HttpStatus.OK);
    }

    @RequestMapping("/test2")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<String> testUser() {
        return new ResponseEntity<String>("Hello, any user!", HttpStatus.OK);
    }
}
