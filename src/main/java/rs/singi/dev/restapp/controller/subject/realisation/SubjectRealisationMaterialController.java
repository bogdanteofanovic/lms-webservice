package rs.singi.dev.restapp.controller.subject.realisation;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationMaterial;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.repository.subject.realisation.SubjectRealisationMaterialRepository;
import rs.singi.dev.restapp.service.subject.realisation.SubjectRealisationMaterialService;

@Controller
@RequestMapping(path = "/api/subjectRealisationMaterial")
public class SubjectRealisationMaterialController extends GenericController<SubjectRealisationMaterial, SubjectRealisationMaterialService, SubjectRealisationMaterialRepository> {
    @RequestMapping(path ="/subjectRealisation/{id}", method = RequestMethod.GET)
    @CrossOrigin
    public ResponseEntity<Iterable<SubjectRealisationMaterial>> findMaterialForRealisation(@PathVariable("id") Long id) {
        return new ResponseEntity<Iterable<SubjectRealisationMaterial>>(service.findBySubjectRealisationIdAndDeletedIsFalse(id), HttpStatus.OK);
    }
}