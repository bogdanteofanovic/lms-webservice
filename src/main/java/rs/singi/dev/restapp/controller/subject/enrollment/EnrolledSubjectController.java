package rs.singi.dev.restapp.controller.subject.enrollment;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.enrollment.EnrolledSubject;
import rs.singi.dev.restapp.repository.subject.enrollment.EnrolledSubjectRepository;
import rs.singi.dev.restapp.service.subject.enrollment.EnrolledSubjectService;


@Controller
@RequestMapping(path = "/api/enrolledSubject")
public class EnrolledSubjectController extends GenericController<EnrolledSubject, EnrolledSubjectService, EnrolledSubjectRepository> {

    @RequestMapping(path ="/student/{id}", method = RequestMethod.GET)
    @CrossOrigin
    public ResponseEntity<Iterable<EnrolledSubject>> findEnrolledSubjectsByStudentId(@PathVariable("id") Long id) {
        return new ResponseEntity<Iterable<EnrolledSubject>>(service.findEnrolledSubjectsByStudentId(id), HttpStatus.OK);
    }

}
