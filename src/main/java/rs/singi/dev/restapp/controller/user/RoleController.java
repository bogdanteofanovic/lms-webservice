package rs.singi.dev.restapp.controller.user;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.model.user.Role;
import rs.singi.dev.restapp.repository.user.RoleRepository;
import rs.singi.dev.restapp.service.user.RoleService;
import rs.singi.dev.restapp.controller.GenericController;
@Controller
@RequestMapping(path = "/api/role")
public class RoleController extends GenericController<Role, RoleService, RoleRepository>{
}