package rs.singi.dev.restapp.controller.subject.realisation;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.realisation.ScheduleRealisation;
import rs.singi.dev.restapp.repository.subject.realisation.ScheduleRealisationRepository;
import rs.singi.dev.restapp.service.subject.realisation.ScheduleRealisationService;

@Controller
@RequestMapping(path = "/api/scheduleRealisation")
public class ScheduleRealisationController extends GenericController<ScheduleRealisation, ScheduleRealisationService, ScheduleRealisationRepository> {
}