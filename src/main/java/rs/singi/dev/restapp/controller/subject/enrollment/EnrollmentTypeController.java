package rs.singi.dev.restapp.controller.subject.enrollment;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.enrollment.EnrollmentType;
import rs.singi.dev.restapp.repository.subject.enrollment.EnrollmentTypeRepository;
import rs.singi.dev.restapp.service.subject.enrollment.EnrollmentTypeService;


@Controller
@RequestMapping(path = "/api/enrollmentType")
public class EnrollmentTypeController extends GenericController<EnrollmentType, EnrollmentTypeService, EnrollmentTypeRepository> {
}
