package rs.singi.dev.restapp.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.dto.UserDto;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.repository.user.UserRepository;
import rs.singi.dev.restapp.service.user.UserService;

import java.util.Optional;

@Controller
@RequestMapping("/api/user")
public class UserController extends GenericController<User, UserService, UserRepository> {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(path ="/free", method = RequestMethod.GET)
    @CrossOrigin
    public ResponseEntity<Iterable<User>> findFreeUsers() {
        return new ResponseEntity<Iterable<User>>(service.findFreeUsers(), HttpStatus.OK);
    }

    @RequestMapping(path = "/activate/{id}", method = RequestMethod.GET)
    @CrossOrigin
    public ResponseEntity<User> getOneByPublicId(@PathVariable("id") String id) {
        Optional<User> ent = service.findByPublicId(id);

        if (ent.isPresent()) {
            ent.get().setActive(true);
            service.save(ent.get());
            return new ResponseEntity<User>(ent.get(), HttpStatus.OK);
        }
        return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/getByLoggedInUser", method = RequestMethod.GET)
    @CrossOrigin
    public ResponseEntity<UserDto> getByLoggedInUser() {
        Optional<User> ent = service.getByUsername(getUserFromToken());
        return ent.map(user -> new ResponseEntity<>(new UserDto(user), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(path = "/changeLoggedInPassword", method = RequestMethod.PUT)
    @CrossOrigin
    public ResponseEntity<UserDto> changeLoggedInPassword(@RequestBody User entity) {
        Optional<User> ent = service.getByUsername(getUserFromToken());

        if (ent.isPresent()) {
            ent.get().setPassword(passwordEncoder.encode(entity.getPassword()));
            service.save(ent.get());
            return new ResponseEntity<UserDto>(new UserDto(ent.get()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(path = "/changeLoggedInEmail", method = RequestMethod.PUT)
    @CrossOrigin
    public ResponseEntity<User> changeLoggedInEmail(@RequestBody User entity) {
        Optional<User> ent = service.getByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        if (ent.isPresent()) {
            ent.get().setEmail(entity.getEmail());
            service.save(ent.get());
            return new ResponseEntity<User>(ent.get(), HttpStatus.OK);
        }
        return new ResponseEntity<User>(HttpStatus.NOT_ACCEPTABLE);
    }

}
