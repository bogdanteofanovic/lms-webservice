package rs.singi.dev.restapp.controller.university;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.University;
import rs.singi.dev.restapp.repository.universtiy.UniversityRepository;
import rs.singi.dev.restapp.service.university.UniversityService;


@Controller
@RequestMapping(path = "/api/university")
public class UniversityController  extends GenericController<University, UniversityService, UniversityRepository> {
}
