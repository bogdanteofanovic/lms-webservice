package rs.singi.dev.restapp.controller.university.faculty.classroom;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.faculty.ClassroomType;
import rs.singi.dev.restapp.repository.universtiy.faculty.ClassroomTypeRepository;
import rs.singi.dev.restapp.service.university.faculty.classroom.ClassroomTypeService;

@Controller
@RequestMapping(path = "/api/classroomType")
public class ClassroomTypeController extends GenericController<ClassroomType, ClassroomTypeService, ClassroomTypeRepository> {
}