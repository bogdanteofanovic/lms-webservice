package rs.singi.dev.restapp.controller.university.faculty.classroom;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.faculty.Classroom;
import rs.singi.dev.restapp.repository.universtiy.faculty.ClassroomRepository;
import rs.singi.dev.restapp.service.university.faculty.classroom.ClassroomService;

@Controller
@RequestMapping(path = "/api/classroom")
public class ClassroomController extends GenericController<Classroom, ClassroomService, ClassroomRepository> {
}