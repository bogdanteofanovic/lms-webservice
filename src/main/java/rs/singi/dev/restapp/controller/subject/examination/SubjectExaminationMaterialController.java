package rs.singi.dev.restapp.controller.subject.examination;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationMaterial;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationMaterial;
import rs.singi.dev.restapp.repository.subject.examination.SubjectExaminationMaterialRepository;
import rs.singi.dev.restapp.service.subject.examination.SubjectExaminationMaterialService;

@Controller
@RequestMapping(path = "/api/subjectExaminationMaterial")
public class SubjectExaminationMaterialController extends GenericController<SubjectExaminationMaterial, SubjectExaminationMaterialService, SubjectExaminationMaterialRepository> {
    @RequestMapping(path ="/subjectExaminationDetails/{id}", method = RequestMethod.GET)
    @CrossOrigin
    public ResponseEntity<Iterable<SubjectExaminationMaterial>> findMaterialForExaminationDetails(@PathVariable("id") Long id) {
        return new ResponseEntity<Iterable<SubjectExaminationMaterial>>(service.findBySubjectExaminationDetailsIdAndDeletedIsFalse(id), HttpStatus.OK);
    }
}