package rs.singi.dev.restapp.controller.employee;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.repository.employee.EmployeeTypeRepository;
import rs.singi.dev.restapp.service.employee.EmployeeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import rs.singi.dev.restapp.model.employee.EmployeeType;


@Controller
@RequestMapping(path = "/api/employeeType")
public class EmployeeTypeController extends GenericController<EmployeeType, EmployeeTypeService, EmployeeTypeRepository> {
}
