package rs.singi.dev.restapp.controller.university.faculty;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.faculty.Staff;
import rs.singi.dev.restapp.repository.universtiy.faculty.StaffRepository;
import rs.singi.dev.restapp.service.university.faculty.StaffService;

@Controller
@RequestMapping(path = "/api/staff")
public class StaffController extends GenericController<Staff, StaffService, StaffRepository> {
}