package rs.singi.dev.restapp.controller.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.address.Country;
import rs.singi.dev.restapp.repository.address.CountryRepository;
import rs.singi.dev.restapp.service.address.CountryService;

@Controller
@RequestMapping(path = "/api/country")
public class CountryController extends GenericController<Country, CountryService, CountryRepository> {
}
