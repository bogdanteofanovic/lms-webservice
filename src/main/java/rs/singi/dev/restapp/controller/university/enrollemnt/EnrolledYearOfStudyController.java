package rs.singi.dev.restapp.controller.university.enrollemnt;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.enrollment.EnrolledYearOfStudy;
import rs.singi.dev.restapp.repository.universtiy.enrollment.EnrolledYearOfStudyRepository;
import rs.singi.dev.restapp.service.university.enrollment.EnrolledYearOfStudyService;

@Controller
@RequestMapping(path = "/api/enrolledYearOfStudy")
public class EnrolledYearOfStudyController extends GenericController<EnrolledYearOfStudy, EnrolledYearOfStudyService, EnrolledYearOfStudyRepository> {
}