package rs.singi.dev.restapp.controller.subject;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.Subject;
import rs.singi.dev.restapp.repository.subject.SubjectRepository;
import rs.singi.dev.restapp.service.subject.SubjectService;


@Controller
@RequestMapping(path = "/api/subject")
public class SubjectController extends GenericController<Subject, SubjectService, SubjectRepository> {
}
