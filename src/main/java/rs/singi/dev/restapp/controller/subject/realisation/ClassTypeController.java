package rs.singi.dev.restapp.controller.subject.realisation;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationType;
import rs.singi.dev.restapp.repository.subject.realisation.SubjectRealisationTypeRepository;
import rs.singi.dev.restapp.service.subject.realisation.SubjectRealisationTypeService;

@Controller
@RequestMapping(path = "/api/classType")
public class ClassTypeController extends GenericController<SubjectRealisationType, SubjectRealisationTypeService, SubjectRealisationTypeRepository> {
}
