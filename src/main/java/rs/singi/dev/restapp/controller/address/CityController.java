package rs.singi.dev.restapp.controller.address;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.address.City;
import rs.singi.dev.restapp.repository.address.CityRepository;
import rs.singi.dev.restapp.service.address.CityService;

@Controller
@RequestMapping(path = "/api/city")
public class CityController extends GenericController<City, CityService, CityRepository> {
}
