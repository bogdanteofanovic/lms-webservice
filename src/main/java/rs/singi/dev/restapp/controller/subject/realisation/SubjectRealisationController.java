package rs.singi.dev.restapp.controller.subject.realisation;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisation;
import rs.singi.dev.restapp.repository.subject.realisation.SubjectRealisationRepository;
import rs.singi.dev.restapp.service.subject.realisation.SubjectRealisationService;


@Controller
@RequestMapping(path = "/api/subjectRealisation")
public class SubjectRealisationController extends GenericController<SubjectRealisation, SubjectRealisationService, SubjectRealisationRepository> {
}
