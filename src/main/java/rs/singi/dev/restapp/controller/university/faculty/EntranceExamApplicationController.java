package rs.singi.dev.restapp.controller.university.faculty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.dto.EntranceExamApplicationDto;
import rs.singi.dev.restapp.model.university.faculty.EntranceExamApplication;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.repository.universtiy.faculty.EntranceExamApplicationRepository;
import rs.singi.dev.restapp.service.university.faculty.EntranceExamApplicationService;
import rs.singi.dev.restapp.service.user.UserService;

import java.util.Optional;

@Controller
@RequestMapping(path = "/api/entranceExamApplication")
public class EntranceExamApplicationController extends GenericController<EntranceExamApplication, EntranceExamApplicationService, EntranceExamApplicationRepository> {

    @Autowired
    UserService userService;


    @RequestMapping(path = "/apply", method = RequestMethod.POST)
    @CrossOrigin
    public ResponseEntity<EntranceExamApplication> apply(@RequestBody EntranceExamApplicationDto entity) {

        Optional<User> user = userService.getByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        EntranceExamApplication entranceExamApplication = new EntranceExamApplication();
        entranceExamApplication.setEntranceExam(entity.getEntranceExam());

        if(user.isPresent()){
            entranceExamApplication.setUser(user.get());
            EntranceExamApplication savedEntity = service.save(entranceExamApplication);
            return new ResponseEntity<EntranceExamApplication>(savedEntity, HttpStatus.CREATED);
        }
        return null;

    }


}
