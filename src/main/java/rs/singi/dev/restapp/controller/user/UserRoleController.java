package rs.singi.dev.restapp.controller.user;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.model.user.UserRole;
import rs.singi.dev.restapp.repository.user.UserRoleRepository;
import rs.singi.dev.restapp.service.user.UserRoleService;
import rs.singi.dev.restapp.controller.GenericController;
@Controller
@RequestMapping(path = "/api/user_role")
public class UserRoleController extends GenericController<UserRole, UserRoleService, UserRoleRepository>{
}