package rs.singi.dev.restapp.controller.file;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import org.springframework.stereotype.Controller;
import rs.singi.dev.restapp.model.file.DBFile;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.service.file.DBFileService;

import java.io.IOException;
import java.time.LocalDateTime;


@Controller
@RequestMapping(path = "/api/DBfile")
public class DBFileController extends AbstractFileController<DBFile, DBFileService> {

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<DBFile>> getAll() {
        return new ResponseEntity<Iterable<DBFile>>(service.findAll(), HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<DBFile> uploadFile(@RequestParam("file") MultipartFile file) throws Exception {
        System.out.println("New file uploaded: " + file.getOriginalFilename());
        User user = userService.getByUsername(getUserFromToken()).orElse(null);
        if (service.getByFileName(file.getOriginalFilename()) != null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        if (user == null || user.getDeleted()) {
            return new ResponseEntity("Employee not found!", HttpStatus.NOT_FOUND);
        }
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (fileName.contains("..")) {
                throw new Exception("The file name contains invalid characters.");
            }
            DBFile dbFile = new DBFile(fileName, file.getContentType(), user, LocalDateTime.now(), LocalDateTime.now(), file.getBytes());
            DBFile newFile = service.storeFile(dbFile);
            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(("/downloadFile/")).path(newFile.getId().toString()).toUriString();
            return new ResponseEntity<DBFile>(newFile, HttpStatus.OK);
        } catch (IOException ex) {
            throw new Exception("This file could not be stored! Please try again.", ex);
        }
    }

    @RequestMapping(path = "/uploadFiles", method = RequestMethod.POST)
    public ResponseEntity<?> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        for (MultipartFile file: files) {
            try {
                uploadFile(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> replaceFile(@RequestParam("file") MultipartFile file, @PathVariable("id") Long id) throws Exception {
        DBFile dbFile = service.findOne(id);
        User user = userService.getByUsername(getUserFromToken()).orElse(null);
        if (user == null || user.getDeleted()) {
            return new ResponseEntity<>("Employee not found!", HttpStatus.NOT_FOUND);
        }
        if (dbFile != null) {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            try {
                if (fileName.contains("..")) {
                    throw new Exception("The file name contains invalid characters.");
                }
                dbFile.setFileName(fileName);
                dbFile.setFileType(file.getContentType());
                dbFile.setUser(user);
                dbFile.setLastModifiedDate(LocalDateTime.now());
                dbFile.setData(file.getBytes());
                service.storeFile(dbFile);
            } catch (IOException ex) {
                throw new Exception("This file could not be stored! Please try again.", ex);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(("/downloadFile/")).path(dbFile.getId().toString()).toUriString();
        return new ResponseEntity<Object>(fileDownloadUri, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<DBFile> getFile(@PathVariable Long id) {
        DBFile dbFile = service.findOne(id);
        if (dbFile == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<DBFile>(dbFile, HttpStatus.OK);
    }

    @RequestMapping(path = "file/{id}", method = RequestMethod.GET)
    public ResponseEntity<Resource> getFileContent(@PathVariable Long id) {
        DBFile dbFile = service.findOne(id);
        if (dbFile == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getData()));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteFile(@PathVariable Long id) {
        DBFile dbFile = service.findOne(id);
        if (dbFile == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        dbFile.setDeleted(true);
        service.storeFile(dbFile);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
