package rs.singi.dev.restapp.controller.university;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.YearOfStudy;
import rs.singi.dev.restapp.repository.universtiy.YearOfStudyRepository;
import rs.singi.dev.restapp.service.university.YearOfStudyService;



@Controller
@RequestMapping(path = "/api/yearOfStudy")
public class YearOfStudyController  extends GenericController<YearOfStudy, YearOfStudyService, YearOfStudyRepository> {
}
