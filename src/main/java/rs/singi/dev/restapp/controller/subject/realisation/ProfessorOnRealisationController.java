package rs.singi.dev.restapp.controller.subject.realisation;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.realisation.ProfessorOnRealisation;
import rs.singi.dev.restapp.repository.subject.realisation.ProfessorOnRealisationRepository;
import rs.singi.dev.restapp.service.subject.realisation.ProfessorOnRealisationService;


@Controller
@RequestMapping(path = "/api/professorOnRealisation")
public class ProfessorOnRealisationController extends GenericController<ProfessorOnRealisation, ProfessorOnRealisationService, ProfessorOnRealisationRepository> {
}
