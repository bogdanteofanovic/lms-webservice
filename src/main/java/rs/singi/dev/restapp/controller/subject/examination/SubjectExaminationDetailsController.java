package rs.singi.dev.restapp.controller.subject.examination;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationDetails;
import rs.singi.dev.restapp.repository.subject.examination.SubjectExaminationDetailsRepository;
import rs.singi.dev.restapp.service.subject.examination.SubjectExaminationDetailsService;

@Controller
@RequestMapping(path = "/api/subjectExaminationDetails")
public class SubjectExaminationDetailsController extends GenericController<SubjectExaminationDetails, SubjectExaminationDetailsService, SubjectExaminationDetailsRepository> {
}