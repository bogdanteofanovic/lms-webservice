package rs.singi.dev.restapp.controller.university.faculty;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.faculty.StudyProgram;
import rs.singi.dev.restapp.repository.universtiy.faculty.StudyProgramRepository;
import rs.singi.dev.restapp.service.university.faculty.StudyProgramService;


@Controller
@RequestMapping(path = "/api/studyProgram")
public class StudyProgramController extends GenericController<StudyProgram, StudyProgramService, StudyProgramRepository> {
}
