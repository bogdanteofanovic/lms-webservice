package rs.singi.dev.restapp.controller.university.faculty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.faculty.EntranceExam;
import rs.singi.dev.restapp.repository.universtiy.faculty.EntranceExamRepository;
import rs.singi.dev.restapp.service.university.faculty.EntranceExamService;

@Controller
@RequestMapping(path = "/api/entranceExam")
public class EntranceExamController extends GenericController<EntranceExam, EntranceExamService, EntranceExamRepository> {
}
