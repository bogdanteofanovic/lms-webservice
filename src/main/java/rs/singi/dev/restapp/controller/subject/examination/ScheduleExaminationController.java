package rs.singi.dev.restapp.controller.subject.examination;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.examination.ScheduleExamination;
import rs.singi.dev.restapp.repository.subject.examination.ScheduleExaminationRepository;
import rs.singi.dev.restapp.service.subject.examination.ScheduleExaminationService;

@Controller
@RequestMapping(path = "/api/scheduleExamination")
public class ScheduleExaminationController extends GenericController<ScheduleExamination, ScheduleExaminationService, ScheduleExaminationRepository> {
}