package rs.singi.dev.restapp.controller.file;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rs.singi.dev.restapp.model.file.AbstractFile;
import rs.singi.dev.restapp.service.file.FSFileService;
import rs.singi.dev.restapp.service.file.GenericFileService;
import rs.singi.dev.restapp.service.user.UserService;

import java.io.IOException;

public abstract class AbstractFileController<T extends AbstractFile, V extends GenericFileService> {
    @Autowired
    protected UserService userService;

    @Autowired
    protected V service;

    @Value("${uploaded.files}")
    protected String uploadsFolder;

    public abstract ResponseEntity<T> uploadFile(@RequestParam("file") MultipartFile file) throws Exception;
    public abstract ResponseEntity<?> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files);
    public abstract ResponseEntity<?> replaceFile(@RequestParam("file") MultipartFile file, @PathVariable("id") Long id) throws Exception;
    public abstract ResponseEntity<T> getFile(@PathVariable Long id) throws IOException;
    public abstract ResponseEntity<?> deleteFile(@PathVariable Long id);


    public String getUserFromToken() {
        Object loggedUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (loggedUser instanceof UserDetails) {
            String username = ((UserDetails)loggedUser).getUsername();
            return username;
        } else {
            String username = loggedUser.toString();
            return username;
        }
    }
}
