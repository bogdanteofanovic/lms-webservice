package rs.singi.dev.restapp.controller.university;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.Student;
import rs.singi.dev.restapp.repository.universtiy.StudentRepository;
import rs.singi.dev.restapp.service.university.StudentService;


@Controller
@RequestMapping(path = "/api/student")
public class StudentController extends GenericController<Student, StudentService, StudentRepository> {
}
