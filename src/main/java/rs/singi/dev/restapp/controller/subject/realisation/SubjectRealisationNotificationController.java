package rs.singi.dev.restapp.controller.subject.realisation;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationNotification;
import rs.singi.dev.restapp.repository.subject.realisation.SubjectRealisationNotificationRepository;
import rs.singi.dev.restapp.service.subject.realisation.SubjectRealisationNotificationService;

@Controller
@RequestMapping(path = "/api/subjectRealisationNotification")
public class SubjectRealisationNotificationController extends GenericController<SubjectRealisationNotification, SubjectRealisationNotificationService, SubjectRealisationNotificationRepository> {
}