package rs.singi.dev.restapp.controller.subject.examination;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.examination.ExaminationRegistration;
import rs.singi.dev.restapp.repository.subject.examination.ExaminationRegistrationRepository;
import rs.singi.dev.restapp.service.subject.examination.ExaminationRegistrationService;

@Controller
@RequestMapping(path = "/api/examinationRegistration")
public class ExaminationRegistrationController extends GenericController<ExaminationRegistration, ExaminationRegistrationService, ExaminationRegistrationRepository> {
}