package rs.singi.dev.restapp.controller.address;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.address.Address;
import rs.singi.dev.restapp.model.file.FSFile;
import rs.singi.dev.restapp.repository.address.AddressRepository;
import rs.singi.dev.restapp.service.address.AddressService;

@Controller
@RequestMapping(path = "/api/address")
public class AddressController extends GenericController<Address, AddressService, AddressRepository> {
}
