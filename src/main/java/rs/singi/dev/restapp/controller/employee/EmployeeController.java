package rs.singi.dev.restapp.controller.employee;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.employee.Employee;
import rs.singi.dev.restapp.repository.employee.EmployeeRepository;
import rs.singi.dev.restapp.service.employee.EmployeeService;


@Controller
@RequestMapping(path = "/api/employee")
public class EmployeeController extends GenericController<Employee, EmployeeService, EmployeeRepository> {
}
