package rs.singi.dev.restapp.controller.subject.examination;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.examination.SubjectExamination;
import rs.singi.dev.restapp.repository.subject.examination.SubjectExaminationRepository;
import rs.singi.dev.restapp.service.subject.examination.SubjectExaminationService;


@Controller
@RequestMapping(path = "/api/subjectExamination")
public class SubjectExaminationController extends GenericController<SubjectExamination, SubjectExaminationService, SubjectExaminationRepository> {
}
