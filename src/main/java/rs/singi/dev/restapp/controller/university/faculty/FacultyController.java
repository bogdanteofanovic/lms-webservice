package rs.singi.dev.restapp.controller.university.faculty;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.faculty.Faculty;
import rs.singi.dev.restapp.repository.universtiy.faculty.FacultyRepository;
import rs.singi.dev.restapp.service.university.faculty.FacultyService;


@Controller
@RequestMapping(path = "/api/faculty")
public class FacultyController extends GenericController<Faculty, FacultyService, FacultyRepository> {
}
