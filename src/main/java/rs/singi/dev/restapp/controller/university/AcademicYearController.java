package rs.singi.dev.restapp.controller.university;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.AcademicYear;
import rs.singi.dev.restapp.repository.universtiy.AcademicYearRepository;
import rs.singi.dev.restapp.service.university.AcademicYearService;

@Controller
@RequestMapping(path = "/api/academicYear")
public class AcademicYearController extends GenericController<AcademicYear, AcademicYearService, AcademicYearRepository> {
}
