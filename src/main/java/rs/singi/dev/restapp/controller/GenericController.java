package rs.singi.dev.restapp.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.repository.CustomRepository;
import rs.singi.dev.restapp.service.GenericService;


public abstract class GenericController<T extends AbstractEntity, V extends GenericService<T, R>, R extends PagingAndSortingRepository<T, Long> & CustomRepository<T>> {

    @Autowired
    protected V service;

    public GenericController() {
    }

    public GenericController(V service) {
        this.service = service;

    }

    @RequestMapping(path ="", method = RequestMethod.GET)
    @CrossOrigin
    public ResponseEntity<Iterable<T>> getAll() {
        return new ResponseEntity<Iterable<T>>(service.findAll(), HttpStatus.OK);
    }

    @RequestMapping(path ="/deleted", method = RequestMethod.GET)
    @CrossOrigin
    public ResponseEntity<Iterable<T>> getAllDeleted() {
        return new ResponseEntity<Iterable<T>>(service.findAllDeleted(), HttpStatus.OK);
    }

    @RequestMapping(path ="/notDeleted", method = RequestMethod.GET)
    @CrossOrigin
    public ResponseEntity<Iterable<T>> getAllNotDeleted() {
        return new ResponseEntity<Iterable<T>>(service.findAllNotDeleted(), HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @CrossOrigin
    public ResponseEntity<T> getOne(@PathVariable("id") Long id) {
        T ent = service.findOne(id);

        if (ent != null) {
            System.out.println(ent);
            return new ResponseEntity<T>(ent, HttpStatus.OK);
        }
        return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
    }


    @RequestMapping(path = "", method = RequestMethod.POST)
    @CrossOrigin
    public ResponseEntity<T> create(@RequestBody T entity) {
        T savedEntity = service.save(entity);
        return new ResponseEntity<T>(savedEntity, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    @CrossOrigin
    public ResponseEntity<T> update(@PathVariable("id") Long id,
                                             @RequestBody T entity) {
        T ent = service.findOne(id);
        if (ent == null) {
            return new ResponseEntity<T>(HttpStatus.NOT_FOUND);
        }
        service.save(entity);
        return new ResponseEntity<T>(entity, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @CrossOrigin
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        T entity = service.findOne(id);
        if (entity == null) {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        entity.setDeleted(true);
        service.save(entity);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = "/restore/{id}", method = RequestMethod.PUT)
    @CrossOrigin
    public ResponseEntity<?> restore(@PathVariable("id") Long id) {
        T entity = service.findOne(id);
        System.out.println("restore");
        if (entity == null) {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        entity.setDeleted(false);
        service.save(entity);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(path = "/delete/{id}", method = RequestMethod.DELETE)
    @CrossOrigin
    public ResponseEntity<?> deletePhysical(@PathVariable("id") Long id) {
        T entity = service.findOne(id);
        if (entity == null) {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        service.delete(id);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }




    public String getUserFromToken() {
        Object loggedUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (loggedUser instanceof UserDetails) {
            String username = ((UserDetails)loggedUser).getUsername();
            return username;
        } else {
            String username = loggedUser.toString();
            return username;
        }
    }
}
