package rs.singi.dev.restapp.controller.university.faculty;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.faculty.StaffType;
import rs.singi.dev.restapp.repository.universtiy.faculty.StaffTypeRepository;
import rs.singi.dev.restapp.service.university.faculty.StaffTypeService;

@Controller
@RequestMapping(path = "/api/staffType")
public class StaffTypeController extends GenericController<StaffType, StaffTypeService, StaffTypeRepository> {
}