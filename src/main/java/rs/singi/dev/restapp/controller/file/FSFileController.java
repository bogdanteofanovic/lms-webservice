package rs.singi.dev.restapp.controller.file;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.model.file.FSFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import org.springframework.core.io.Resource;
import rs.singi.dev.restapp.service.file.FSFileService;


@Controller
@RequestMapping(path = "/api/FSfile")
public class FSFileController extends AbstractFileController<FSFile, FSFileService> {

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<Iterable<FSFile>> getAll() {
        return new ResponseEntity<Iterable<FSFile>>(service.findAll(), HttpStatus.OK);
    }

    @RequestMapping(path = "/deleted", method = RequestMethod.GET)
    public ResponseEntity<Iterable<FSFile>> getAllDeleted() {
        return new ResponseEntity<Iterable<FSFile>>(service.findAllDeleted(), HttpStatus.OK);
    }

    @RequestMapping(path = "/user/{username}", method = RequestMethod.GET)
    public ResponseEntity<Iterable<FSFile>> getAllByUserUsername(@PathVariable("username") String username) {
        return new ResponseEntity<Iterable<FSFile>>(service.findByUserUsername(username), HttpStatus.OK);
    }

//    @RequestMapping(path = "/subjectRealisation/{id}", method = RequestMethod.GET)
//    public ResponseEntity<Iterable<FSFile>> getAllForSubjectRealisation(@PathVariable("id") Long id) {
//        return new ResponseEntity<Iterable<FSFile>>(service.findAllForSubjectRealisation(id), HttpStatus.OK);
//    }
//
//    @RequestMapping(path = "/subjectExamination/{id}", method = RequestMethod.GET)
//    public ResponseEntity<Iterable<FSFile>> getAllForSubjectExamination(@PathVariable("id") Long id) {
//        return new ResponseEntity<Iterable<FSFile>>(service.findAllForSubjectExamination(id), HttpStatus.OK);
//    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<FSFile> uploadFile(@RequestParam("file") MultipartFile file) throws Exception {
        System.out.println("Uploading...: " + file.getOriginalFilename());
        User user = userService.getByUsername(getUserFromToken()).orElse(null);
        if (service.getByFilePath(this.uploadsFolder+"/"+file.getOriginalFilename()).isPresent()) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        if (user == null || user.getDeleted()) {
            return new ResponseEntity("Employee not found!", HttpStatus.NOT_FOUND);
        }
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (fileName.contains("..")) {
                throw new Exception("The file name contains invalid characters.");
            }
            file.transferTo(Paths.get(uploadsFolder + "/" + fileName));

            FSFile fsFile = new FSFile(fileName, file.getContentType(), user, LocalDateTime.now(), LocalDateTime.now(), this.uploadsFolder+"/"+file.getOriginalFilename());
            FSFile newFile = service.storeFile(fsFile);
            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(("/downloadFile/")).path(newFile.getId().toString()).toUriString();
            return new ResponseEntity<FSFile>(newFile, HttpStatus.OK);
        } catch (IOException ex) {
            throw new Exception("This file could not be stored! Please try again.", ex);
        }
    }

    @RequestMapping(path = "/uploadFiles", method = RequestMethod.POST)
    public ResponseEntity<?> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        for (MultipartFile file: files) {
            try {
                uploadFile(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

//    @RequestMapping(path ="", method = RequestMethod.GET)
//    @CrossOrigin
//    public ResponseEntity<Iterable<FSFile>> getAll() {
//        return new ResponseEntity<Iterable<FSFile>>(service.findAll(), HttpStatus.OK);
//    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> replaceFile(@RequestParam("file") MultipartFile file, @PathVariable("id") Long id) throws Exception {
        FSFile fsFile = service.findOne(id);
        User user = userService.getByUsername(getUserFromToken()).orElse(null);
        if (user == null || user.getDeleted()) {
            return new ResponseEntity<>("Employee not found!", HttpStatus.NOT_FOUND);
        }
        if (fsFile != null) {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            try {
                if (fileName.contains("..")) {
                    throw new Exception("The file name contains invalid characters.");
                }
                file.transferTo(Paths.get(uploadsFolder + "/" + fileName));

                fsFile.setFileName(fileName);
                fsFile.setFileType(file.getContentType());
                fsFile.setFilePath(this.uploadsFolder+"/"+file.getOriginalFilename());
                fsFile.setLastModifiedDate(LocalDateTime.now());
                fsFile.setUser(user);
                service.storeFile(fsFile);
            } catch (IOException ex) {
                throw new Exception("This file could not be stored! Please try again.", ex);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(("/downloadFile/")).path(fsFile.getId().toString()).toUriString();
        return new ResponseEntity<Object>(fileDownloadUri, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<FSFile> getFile(@PathVariable Long id) throws IOException {
        FSFile fsFile = service.findOne(id);
        if (fsFile == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        byte[] array = Files.readAllBytes(Paths.get(this.uploadsFolder + "/" + fsFile.getFileName()));
        System.out.println(array);
        fsFile.setData(array);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path(("/api/FSfile/downloadFile/")).path(fsFile.getId().toString()).toUriString();
        return new ResponseEntity<FSFile>(fsFile, HttpStatus.OK);
    }

    @RequestMapping(path = "file/{id}", method = RequestMethod.GET)
    public ResponseEntity<Resource> getFileContent(@PathVariable String filename) {
        Resource file = service.loadContent(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteFile(@PathVariable Long id){
        FSFile fsFile = service.findOne(id);
        if (fsFile == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        fsFile.setDeleted(true);
        service.storeFile(fsFile);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
