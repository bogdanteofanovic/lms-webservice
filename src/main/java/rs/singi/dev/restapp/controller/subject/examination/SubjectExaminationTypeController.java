package rs.singi.dev.restapp.controller.subject.examination;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationType;
import rs.singi.dev.restapp.repository.subject.examination.SubjectExaminationTypeRepository;
import rs.singi.dev.restapp.service.subject.examination.SubjectExaminationTypeService;

@Controller
@RequestMapping(path = "/api/subjectExaminationType")
public class SubjectExaminationTypeController extends GenericController<SubjectExaminationType, SubjectExaminationTypeService, SubjectExaminationTypeRepository> {
}