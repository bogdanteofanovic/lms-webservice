package rs.singi.dev.restapp.controller.university.enrollemnt;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import rs.singi.dev.restapp.controller.GenericController;
import rs.singi.dev.restapp.model.university.enrollment.EnrollmentYearType;
import rs.singi.dev.restapp.repository.universtiy.enrollment.EnrollmentYearTypeRepository;
import rs.singi.dev.restapp.service.university.enrollment.EnrollmentYearTypeService;

@Controller
@RequestMapping(path = "/api/enrollmentYearType")
public class EnrollmentYearTypeController extends GenericController<EnrollmentYearType, EnrollmentYearTypeService, EnrollmentYearTypeRepository> {
}