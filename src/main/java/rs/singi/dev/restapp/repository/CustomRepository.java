package rs.singi.dev.restapp.repository;

import rs.singi.dev.restapp.model.AbstractEntity;

public interface CustomRepository<T extends AbstractEntity> {
    public Iterable<T> findAllByDeletedFalse();
    public Iterable<T> findAllByDeletedTrue();
}
