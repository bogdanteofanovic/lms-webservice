package rs.singi.dev.restapp.repository.subject.enrollment;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.singi.dev.restapp.model.employee.EmployeeType;
import rs.singi.dev.restapp.model.subject.enrollment.EnrolledSubject;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface EnrolledSubjectRepository  extends PagingAndSortingRepository<EnrolledSubject, Long>, CustomRepository<EnrolledSubject> {

    @Query("SELECT et FROM EnrolledSubject et, Student s where et.student = s and s.id = ?1")
    Iterable<EnrolledSubject> findEnrolledSubjectsWhereStudentId(Long id);
}
