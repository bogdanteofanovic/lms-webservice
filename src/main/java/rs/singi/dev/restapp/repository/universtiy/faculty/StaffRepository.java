package rs.singi.dev.restapp.repository.universtiy.faculty;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.university.faculty.Faculty;
import rs.singi.dev.restapp.model.university.faculty.Staff;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface StaffRepository extends PagingAndSortingRepository<Staff, Long>, CustomRepository<Staff> {
}
