package rs.singi.dev.restapp.repository.file;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.file.DBFile;
import rs.singi.dev.restapp.model.file.FSFile;

import java.util.Optional;

@Repository
public interface DBFileRepository extends JpaRepository<DBFile, Long> {
    Iterable<DBFile> getByFileName(String fileName);
    @Query("SELECT fileName from DBFile")
    Iterable<String[]> getAllFileNames();
    @Query("SELECT id, deleted, fileType, createdDate, fileName, lastModifiedDate, user from DBFile")
    Iterable<DBFile> findByDeletedIsFalse();
    Iterable<DBFile> findByDeletedIsTrue();
}
