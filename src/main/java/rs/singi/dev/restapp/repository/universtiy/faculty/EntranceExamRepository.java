package rs.singi.dev.restapp.repository.universtiy.faculty;

import org.springframework.data.repository.PagingAndSortingRepository;
import rs.singi.dev.restapp.model.university.faculty.EntranceExam;
import rs.singi.dev.restapp.repository.CustomRepository;

public interface EntranceExamRepository extends PagingAndSortingRepository<EntranceExam, Long>, CustomRepository<EntranceExam> {
}
