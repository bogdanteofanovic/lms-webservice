package rs.singi.dev.restapp.repository.subject.examination;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.subject.enrollment.EnrollmentType;
import rs.singi.dev.restapp.model.subject.examination.ExaminationRegistration;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface ExaminationRegistrationRepository extends PagingAndSortingRepository<ExaminationRegistration, Long>, CustomRepository<ExaminationRegistration> {
}
