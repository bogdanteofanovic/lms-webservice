package rs.singi.dev.restapp.repository.subject.enrollment;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.singi.dev.restapp.model.subject.enrollment.EnrolledSubject;
import rs.singi.dev.restapp.model.subject.enrollment.EnrollmentType;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface EnrollmentTypeRepository  extends PagingAndSortingRepository<EnrollmentType, Long>, CustomRepository<EnrollmentType> {
}
