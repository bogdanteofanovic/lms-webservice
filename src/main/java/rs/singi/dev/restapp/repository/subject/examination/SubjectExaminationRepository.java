package rs.singi.dev.restapp.repository.subject.examination;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.subject.examination.SubjectExamination;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationMaterial;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface SubjectExaminationRepository extends PagingAndSortingRepository<SubjectExamination, Long>, CustomRepository<SubjectExamination> {
}
