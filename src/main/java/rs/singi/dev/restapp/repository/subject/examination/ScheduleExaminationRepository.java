package rs.singi.dev.restapp.repository.subject.examination;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.subject.examination.ExaminationRegistration;
import rs.singi.dev.restapp.model.subject.examination.ScheduleExamination;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface ScheduleExaminationRepository extends PagingAndSortingRepository<ScheduleExamination, Long>, CustomRepository<ScheduleExamination> {
}
