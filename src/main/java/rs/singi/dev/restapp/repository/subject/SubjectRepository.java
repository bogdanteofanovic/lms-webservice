package rs.singi.dev.restapp.repository.subject;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.singi.dev.restapp.model.subject.Subject;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface SubjectRepository  extends PagingAndSortingRepository<Subject, Long>, CustomRepository<Subject> {
}
