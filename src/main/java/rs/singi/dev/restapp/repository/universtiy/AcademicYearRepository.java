package rs.singi.dev.restapp.repository.universtiy;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.singi.dev.restapp.model.university.AcademicYear;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface AcademicYearRepository  extends PagingAndSortingRepository<AcademicYear, Long>, CustomRepository<AcademicYear> {
}
