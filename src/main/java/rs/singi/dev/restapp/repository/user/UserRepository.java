package rs.singi.dev.restapp.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.user.Role;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.repository.CustomRepository;

import java.util.Optional;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> , CustomRepository<User> {

    Optional<User> getByUsername(String username);
    Optional<User> getByUsernameAndPassword(String username, String password);
    Optional<User> getByEmail(String email);
    Iterable<User> findUsersByEmployeeIsNullAndStudentIsNull();
    Optional<User> findByPublicId(String publicId);
}
