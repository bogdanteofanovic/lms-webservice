package rs.singi.dev.restapp.repository.file;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.file.DBFile;
import rs.singi.dev.restapp.model.file.FSFile;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.repository.CustomRepository;

import java.util.Optional;

@Repository
public interface FSFileRepository extends PagingAndSortingRepository<FSFile, Long>, CustomRepository<FSFile> {
    Optional<FSFile> getByFilePath(String filePath);
    @Query("SELECT fileName from FSFile")
    Iterable<String[]> getAllFileNames();
    @Query("SELECT f from FSFile f, User u where f.user = u.id and u.username = :username AND f.deleted = false")
    Iterable<FSFile> findByUserUsername(@Param("username") String username);
//    @Query("SELECT f from FSFile f, SubjectRealisationMaterial sm, SubjectRealisation r where sm.fsFile = f.id AND sm.subjectRealisation = r.id AND f.deleted = false AND r.id = :id")
//    Iterable<FSFile> findAllForSubjectRealisation(@Param("id") Long id);
//    @Query("SELECT f from FSFile f, SubjectExaminationMaterial sm, SubjectExaminationDetails r where sm.fsFile = f.id AND sm.subjectExaminationDetails = r.id AND f.deleted = false AND r.id = :id")
//    Iterable<FSFile> findAllForSubjectExamination(@Param("id") Long id);
}
