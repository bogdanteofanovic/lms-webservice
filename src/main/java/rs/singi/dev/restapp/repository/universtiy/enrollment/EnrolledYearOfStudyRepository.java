package rs.singi.dev.restapp.repository.universtiy.enrollment;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.subject.Subject;
import rs.singi.dev.restapp.model.subject.enrollment.EnrolledSubject;
import rs.singi.dev.restapp.model.university.enrollment.EnrolledYearOfStudy;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface EnrolledYearOfStudyRepository extends PagingAndSortingRepository<EnrolledYearOfStudy, Long>, CustomRepository<EnrolledYearOfStudy> {
}
