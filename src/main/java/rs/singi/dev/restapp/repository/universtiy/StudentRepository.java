package rs.singi.dev.restapp.repository.universtiy;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.singi.dev.restapp.model.university.AcademicYear;
import rs.singi.dev.restapp.model.university.Student;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.repository.CustomRepository;

import java.util.Optional;

@Repository
public interface StudentRepository  extends PagingAndSortingRepository<Student, Long>, CustomRepository<Student> {
    Optional<Student> findByUser(User u);
}
