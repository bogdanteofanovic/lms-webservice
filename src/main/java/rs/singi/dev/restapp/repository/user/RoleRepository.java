package rs.singi.dev.restapp.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.university.YearOfStudy;
import rs.singi.dev.restapp.model.user.Role;
import rs.singi.dev.restapp.repository.CustomRepository;

import java.util.Optional;

@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, Long>, CustomRepository<Role> {
    Optional<Role> getByName(String name);
}
