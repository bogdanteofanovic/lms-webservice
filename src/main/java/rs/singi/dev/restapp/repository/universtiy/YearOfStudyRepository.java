package rs.singi.dev.restapp.repository.universtiy;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.singi.dev.restapp.model.university.University;
import rs.singi.dev.restapp.model.university.YearOfStudy;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface YearOfStudyRepository  extends PagingAndSortingRepository<YearOfStudy, Long>, CustomRepository<YearOfStudy> {
}
