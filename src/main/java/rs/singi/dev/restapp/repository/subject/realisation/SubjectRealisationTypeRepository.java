package rs.singi.dev.restapp.repository.subject.realisation;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisation;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationType;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface SubjectRealisationTypeRepository extends PagingAndSortingRepository<SubjectRealisationType, Long>, CustomRepository<SubjectRealisationType> {
}
