package rs.singi.dev.restapp.repository.subject.examination;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationDetails;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationMaterial;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationMaterial;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface SubjectExaminationMaterialRepository extends PagingAndSortingRepository<SubjectExaminationMaterial, Long>, CustomRepository<SubjectExaminationMaterial> {
    Iterable<SubjectExaminationMaterial> findBySubjectExaminationDetailsIdAndDeletedIsFalse(Long id);
}
