package rs.singi.dev.restapp.repository.subject.examination;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.subject.examination.ScheduleExamination;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationDetails;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface SubjectExaminationDetailsRepository extends PagingAndSortingRepository<SubjectExaminationDetails, Long>, CustomRepository<SubjectExaminationDetails> {
}
