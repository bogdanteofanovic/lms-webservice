package rs.singi.dev.restapp.repository.subject.realisation;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationMaterial;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationNotification;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface SubjectRealisationNotificationRepository extends PagingAndSortingRepository<SubjectRealisationNotification, Long>, CustomRepository<SubjectRealisationNotification> {
}
