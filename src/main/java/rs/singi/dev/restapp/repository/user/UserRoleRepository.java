package rs.singi.dev.restapp.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.user.User;
import rs.singi.dev.restapp.model.user.UserRole;
import rs.singi.dev.restapp.repository.CustomRepository;

import java.util.Set;

@Repository
public interface UserRoleRepository extends PagingAndSortingRepository<UserRole, Long>, CustomRepository<UserRole> {
    Set<UserRole> getByUserId(Long id);
}
