package rs.singi.dev.restapp.repository.subject.realisation;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationType;
import rs.singi.dev.restapp.model.subject.realisation.ProfessorOnRealisation;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface ProfessorOnRealisationRepository  extends PagingAndSortingRepository<ProfessorOnRealisation, Long>, CustomRepository<ProfessorOnRealisation> {
}
