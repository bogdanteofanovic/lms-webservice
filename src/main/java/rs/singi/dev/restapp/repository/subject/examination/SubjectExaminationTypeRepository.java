package rs.singi.dev.restapp.repository.subject.examination;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.subject.examination.SubjectExamination;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationType;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface SubjectExaminationTypeRepository extends PagingAndSortingRepository<SubjectExaminationType, Long>, CustomRepository<SubjectExaminationType> {
}
