package rs.singi.dev.restapp.repository.universtiy.faculty;

import org.springframework.data.repository.PagingAndSortingRepository;
import rs.singi.dev.restapp.model.university.faculty.EntranceExamApplication;
import rs.singi.dev.restapp.repository.CustomRepository;

public interface EntranceExamApplicationRepository extends PagingAndSortingRepository<EntranceExamApplication, Long>, CustomRepository<EntranceExamApplication> {
}
