package rs.singi.dev.restapp.repository.universtiy.faculty;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.university.faculty.Staff;
import rs.singi.dev.restapp.model.university.faculty.StaffType;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface StaffTypeRepository extends PagingAndSortingRepository<StaffType, Long>, CustomRepository<StaffType> {
}
