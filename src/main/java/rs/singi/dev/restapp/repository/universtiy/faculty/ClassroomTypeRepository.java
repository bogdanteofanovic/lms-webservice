package rs.singi.dev.restapp.repository.universtiy.faculty;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.university.faculty.Classroom;
import rs.singi.dev.restapp.model.university.faculty.ClassroomType;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface ClassroomTypeRepository extends PagingAndSortingRepository<ClassroomType, Long>, CustomRepository<ClassroomType> {
}
