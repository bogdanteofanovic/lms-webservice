package rs.singi.dev.restapp.repository.address;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.singi.dev.restapp.model.address.Address;
import rs.singi.dev.restapp.model.address.City;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface CityRepository extends PagingAndSortingRepository<City, Long> , CustomRepository<City> {
}

