package rs.singi.dev.restapp.repository.universtiy;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.singi.dev.restapp.model.university.Student;
import rs.singi.dev.restapp.model.university.University;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface UniversityRepository  extends PagingAndSortingRepository<University, Long>, CustomRepository<University> {
}
