package rs.singi.dev.restapp.repository.employee;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.singi.dev.restapp.model.address.Country;
import rs.singi.dev.restapp.model.employee.Employee;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long>, CustomRepository<Employee> {
}

