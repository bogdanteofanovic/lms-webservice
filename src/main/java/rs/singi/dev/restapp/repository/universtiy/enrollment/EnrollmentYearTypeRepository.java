package rs.singi.dev.restapp.repository.universtiy.enrollment;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import rs.singi.dev.restapp.model.university.enrollment.EnrolledYearOfStudy;
import rs.singi.dev.restapp.model.university.enrollment.EnrollmentYearType;
import rs.singi.dev.restapp.repository.CustomRepository;

@Repository
public interface EnrollmentYearTypeRepository extends PagingAndSortingRepository<EnrollmentYearType, Long>, CustomRepository<EnrollmentYearType> {
}
