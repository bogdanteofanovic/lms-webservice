package rs.singi.dev.restapp.model.university.faculty;

import rs.singi.dev.restapp.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class Classroom extends AbstractEntity {

	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
    private Integer seats;

    @ManyToOne(fetch = FetchType.LAZY)
    private ClassroomType classroomType;

    @ManyToOne(fetch = FetchType.LAZY)
    private Faculty faculty;

    public Classroom() {
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public ClassroomType getClassroomType() {
        return classroomType;
    }

    public void setClassroomType(ClassroomType classroomType) {
        this.classroomType = classroomType;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
