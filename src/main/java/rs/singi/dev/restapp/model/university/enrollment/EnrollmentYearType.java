package rs.singi.dev.restapp.model.university.enrollment;

import rs.singi.dev.restapp.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class EnrollmentYearType extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    public EnrollmentYearType() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
