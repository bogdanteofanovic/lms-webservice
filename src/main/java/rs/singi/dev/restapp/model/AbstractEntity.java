package rs.singi.dev.restapp.model;

import javax.persistence.*;

@MappedSuperclass
public abstract class AbstractEntity {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false, columnDefinition = "BIGINT")
    private Long id;

	@Version
	@Column(nullable = false)
	private Long version;

	@Column(nullable = false, columnDefinition = "TINYINT(1)")
	private Boolean deleted = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	protected void setVersion(Long version) {
		this.version = version;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

}
