package rs.singi.dev.restapp.model.subject.realisation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class SubjectRealisationType extends AbstractEntity {
	@Column(nullable = false)
	private String name;

	@JsonIgnore
	@OneToMany(mappedBy = "subjectRealisationType")
	List<SubjectRealisation> subjectRealisations = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SubjectRealisation> getSubjectRealisations() {
		return subjectRealisations;
	}

	public void setSubjectRealisations(List<SubjectRealisation> subjectRealisations) {
		this.subjectRealisations = subjectRealisations;
	}

	public SubjectRealisationType(String name) {
		this.name = name;
	}

	public SubjectRealisationType() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
}
