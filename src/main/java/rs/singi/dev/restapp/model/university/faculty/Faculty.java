package rs.singi.dev.restapp.model.university.faculty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.address.Address;
import rs.singi.dev.restapp.model.employee.Employee;
import rs.singi.dev.restapp.model.university.University;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Faculty extends AbstractEntity {

	@Column(nullable = false)
	private String name;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private University university;
	
	@OneToOne(optional = false)
	private Address address;
	
	@Column(nullable = false)
	private String phoneNumber;
	
	@Column(nullable = false)
	private String email;

	@Column(nullable = false)
	private String description;

	@JsonIgnore
	@OneToMany(mappedBy = "faculty")
	private List<StudyProgram> studyPrograms = new ArrayList<>();

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	private Employee dean;

	@JsonIgnore
	@OneToMany(mappedBy = "faculty")
	private List<Classroom> classrooms = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<StudyProgram> getStudyPrograms() {
		return studyPrograms;
	}

	public void setStudyPrograms(List<StudyProgram> studyPrograms) {
		this.studyPrograms = studyPrograms;
	}

	public Employee getDean() {
		return dean;
	}

	public void setDean(Employee dean) {
		this.dean = dean;
	}

	public List<Classroom> getClassrooms() {
		return classrooms;
	}

	public void setClassrooms(List<Classroom> classrooms) {
		this.classrooms = classrooms;
	}

	public Faculty(String name, University university, Address address, String phoneNumber, String email, String description, Employee dean) {
		this.name = name;
		this.university = university;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.description = description;
		this.dean = dean;
	}

	public Faculty() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
