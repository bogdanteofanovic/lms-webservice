package rs.singi.dev.restapp.model.university.faculty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.employee.Employee;
import rs.singi.dev.restapp.model.university.YearOfStudy;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class StudyProgram extends AbstractEntity {


	@Column(nullable = false)
	private String name;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Faculty faculty;

	@JsonIgnore
	@OneToMany(mappedBy = "studyProgram")
	List<YearOfStudy> yearOfStudies = new ArrayList<>();

	@Column(nullable = false)
	private String description;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Employee studyProgramDirector;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Faculty getFaculty() {
		return faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public List<YearOfStudy> getYearOfStudies() {
		return yearOfStudies;
	}

	public void setYearOfStudies(List<YearOfStudy> yearOfStudies) {
		this.yearOfStudies = yearOfStudies;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Employee getStudyProgramDirector() {
		return studyProgramDirector;
	}

	public void setStudyProgramDirector(Employee studyProgramDirector) {
		this.studyProgramDirector = studyProgramDirector;
	}

	public StudyProgram(String name, Faculty faculty, String description, Employee studyProgramDirector) {
		this.name = name;
		this.faculty = faculty;
		this.description = description;
		this.studyProgramDirector = studyProgramDirector;
	}

	public StudyProgram() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
