package rs.singi.dev.restapp.model.university.faculty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class StaffType extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "staffType")
    private List<Staff> staff;

    public StaffType() {
    }

    public StaffType(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Staff> getStaff() {
        return staff;
    }

    public void setStaff(List<Staff> staff) {
        this.staff = staff;
    }
}
