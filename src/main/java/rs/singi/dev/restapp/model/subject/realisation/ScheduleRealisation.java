package rs.singi.dev.restapp.model.subject.realisation;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.university.faculty.Classroom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

/**
 * Raspored predavanja
 */
@Entity
public class ScheduleRealisation extends AbstractEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Classroom classroom;

    @Column
    private LocalDateTime startTime;

    @Column
    private LocalDateTime endTime;

    @ManyToOne(fetch = FetchType.LAZY)
    private SubjectRealisation subjectRealisation;

    public ScheduleRealisation() {
    }

    public Classroom getClassroom() {
        return classroom;
    }

    public void setClassroom(Classroom classroom) {
        this.classroom = classroom;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public SubjectRealisation getSubjectRealisation() {
        return subjectRealisation;
    }

    public void setSubjectRealisation(SubjectRealisation subjectRealisation) {
        this.subjectRealisation = subjectRealisation;
    }
}
