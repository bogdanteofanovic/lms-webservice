package rs.singi.dev.restapp.model.university;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.address.Address;
import rs.singi.dev.restapp.model.subject.enrollment.EnrolledSubject;
import rs.singi.dev.restapp.model.subject.examination.ExaminationRegistration;
import rs.singi.dev.restapp.model.subject.examination.SubjectExamination;
import rs.singi.dev.restapp.model.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Student extends AbstractEntity {


	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String lastName;

	@Column(nullable = false, length = 10)
	private Integer indexNumber;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Address address;

	@JsonIgnore
	@OneToMany(mappedBy = "student")
	private List<SubjectExamination> subjectExaminations = new ArrayList<>();

	@JsonIgnore
	@OneToMany(mappedBy = "student")
	private List<EnrolledSubject> enrolledSubjects = new ArrayList<>();

	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@JsonIgnore
	@OneToMany(mappedBy = "student")
	private List<ExaminationRegistration> examinationRegistrations = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(Integer indexNumber) {
		this.indexNumber = indexNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<SubjectExamination> getSubjectExaminations() {
		return subjectExaminations;
	}

	public void setSubjectExaminations(List<SubjectExamination> subjectExaminations) {
		this.subjectExaminations = subjectExaminations;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<ExaminationRegistration> getExaminationRegistrations() {
		return examinationRegistrations;
	}

	public void setExaminationRegistrations(List<ExaminationRegistration> examinationRegistrations) {
		this.examinationRegistrations = examinationRegistrations;
	}

	public Student(String name, String lastName, Integer indexNumber, String email, Address address, User user) {
		this.name = name;
		this.lastName = lastName;
		this.indexNumber = indexNumber;
		this.address = address;
		this.user = user;
	}

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}



}
