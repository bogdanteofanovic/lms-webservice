package rs.singi.dev.restapp.model.subject.examination;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.university.Student;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * Klasa koj abradjuje odredjenu evaluaciju i ostvarene bodove studenta na njoj
 */
@Entity
public class SubjectExamination extends AbstractEntity {

	@ManyToOne(optional = false)
	private SubjectExaminationDetails subjectExaminationDetails;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Student student;

	@Column(nullable = false)
	private Float score;


	public SubjectExamination() {
	}

	public SubjectExaminationDetails getSubjectExaminationDetails() {
		return subjectExaminationDetails;
	}

	public void setSubjectExaminationDetails(SubjectExaminationDetails subjectExaminationDetails) {
		this.subjectExaminationDetails = subjectExaminationDetails;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}
}
