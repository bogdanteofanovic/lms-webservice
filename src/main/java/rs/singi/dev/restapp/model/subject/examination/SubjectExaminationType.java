package rs.singi.dev.restapp.model.subject.examination;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasa kao Tip evaluacije, ostavljamo korisniku softvera da doda razlicite tipove evaluacije ... standardni su Elektronski, Papir, ...
 */
@Entity
public class SubjectExaminationType extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "subjectExaminationType")
    private List<SubjectExaminationDetails> subjectExaminationDetails = new ArrayList<>();

    public SubjectExaminationType() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SubjectExaminationDetails> getSubjectExaminationDetails() {
        return subjectExaminationDetails;
    }

    public void setSubjectExaminationDetails(List<SubjectExaminationDetails> subjectExaminationDetails) {
        this.subjectExaminationDetails = subjectExaminationDetails;
    }
}
