package rs.singi.dev.restapp.model.address;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class City extends AbstractEntity {
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private int zipCode;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Country country;

	@JsonIgnore
	@OneToMany(mappedBy = "city")
	List<Address> addresses = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public City(String name, int zipCode, Country country) {
		this.name = name;
		this.zipCode = zipCode;
		this.country = country;
	}

	public City() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
