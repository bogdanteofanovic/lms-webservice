package rs.singi.dev.restapp.model.university.faculty;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.user.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public class EntranceExamApplication extends AbstractEntity {

    @ManyToOne(optional = false)
    private User user;

    @Column(nullable = false)
    private LocalDateTime dateOfApplication;

    @ManyToOne(optional = false)
    private EntranceExam entranceExam;

    public EntranceExamApplication() {
        this.dateOfApplication = LocalDateTime.now();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getDateOfApplication() {
        return dateOfApplication;
    }

    public void setDateOfApplication(LocalDateTime dateOfApplication) {
        this.dateOfApplication = dateOfApplication;
    }

    public EntranceExam getEntranceExam() {
        return entranceExam;
    }

    public void setEntranceExam(EntranceExam entranceExam) {
        this.entranceExam = entranceExam;
    }
}
