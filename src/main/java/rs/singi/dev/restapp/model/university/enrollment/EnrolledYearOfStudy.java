package rs.singi.dev.restapp.model.university.enrollment;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.university.AcademicYear;
import rs.singi.dev.restapp.model.university.Student;
import rs.singi.dev.restapp.model.university.YearOfStudy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public class EnrolledYearOfStudy extends AbstractEntity {

    @ManyToOne(optional = false)
    private YearOfStudy yearOfStudy;

    @ManyToOne(optional = false)
    private Student student;

    @ManyToOne(optional = false)
    private AcademicYear academicYear;

    @ManyToOne(optional = false)
    private EnrollmentYearType enrollmentYearType;

    @Column(nullable = false)
    private LocalDateTime dateOfEnrollment;

    public EnrolledYearOfStudy() {
        this.dateOfEnrollment = LocalDateTime.now();
    }

    public YearOfStudy getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(YearOfStudy yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public AcademicYear getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(AcademicYear academicYear) {
        this.academicYear = academicYear;
    }

    public EnrollmentYearType getEnrollmentYearType() {
        return enrollmentYearType;
    }

    public void setEnrollmentYearType(EnrollmentYearType enrollmentYearType) {
        this.enrollmentYearType = enrollmentYearType;
    }

    public LocalDateTime getDateOfEnrollment() {
        return dateOfEnrollment;
    }

    public void setDateOfEnrollment(LocalDateTime dateOfEnrollment) {
        this.dateOfEnrollment = dateOfEnrollment;
    }
}
