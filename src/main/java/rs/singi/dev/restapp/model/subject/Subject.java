package rs.singi.dev.restapp.model.subject;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.university.YearOfStudy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class Subject extends AbstractEntity {
	@Column(nullable = false)
	private String name;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private YearOfStudy yearOfStudy;
	
	@Column(nullable = false)
	private int score;

	@Column(nullable = false, columnDefinition="TEXT")
	private String syllabus;

	public Subject() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Subject(String name, YearOfStudy yearOfStudy, int score, String syllabus) {
		this.name = name;
		this.yearOfStudy = yearOfStudy;
		this.score = score;
		this.syllabus = syllabus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public YearOfStudy getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(YearOfStudy yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getSyllabus() {
		return syllabus;
	}

	public void setSyllabus(String syllabus) {
		this.syllabus = syllabus;
	}
}
