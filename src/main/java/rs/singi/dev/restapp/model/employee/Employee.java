package rs.singi.dev.restapp.model.employee;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.subject.realisation.ProfessorOnRealisation;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationNotification;
import rs.singi.dev.restapp.model.university.faculty.Faculty;
import rs.singi.dev.restapp.model.university.faculty.StudyProgram;
import rs.singi.dev.restapp.model.address.Address;
import rs.singi.dev.restapp.model.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Employee extends AbstractEntity {
	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String lastName;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private EmployeeType employeeType;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Address address;

	@JsonIgnore
	@OneToMany(mappedBy = "professor")
	private List<ProfessorOnRealisation> professorOnRealisations = new ArrayList<>();

	@JsonIgnore
	@OneToMany(mappedBy = "dean")
	private List<Faculty> faculties = new ArrayList<>();

	@JsonIgnore
	@OneToMany(mappedBy = "studyProgramDirector")
	private List<StudyProgram> studyPrograms = new ArrayList<>();

	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@JsonIgnore
	@OneToMany(mappedBy = "publisher")
	private List<SubjectRealisationNotification> subjectRealisationNotifications = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String last_name) {
		this.lastName = last_name;
	}

	public EmployeeType getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(EmployeeType employee_type) {
		this.employeeType = employee_type;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<ProfessorOnRealisation> getProfessorOnRealisations() {
		return professorOnRealisations;
	}

	public void setProfessorOnRealisations(List<ProfessorOnRealisation> professorOnRealisations) {
		this.professorOnRealisations = professorOnRealisations;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Faculty> getFaculties() {
		return faculties;
	}

	public void setFaculties(List<Faculty> faculties) {
		this.faculties = faculties;
	}

	public List<StudyProgram> getStudyPrograms() {
		return studyPrograms;
	}

	public void setStudyPrograms(List<StudyProgram> studyPrograms) {
		this.studyPrograms = studyPrograms;
	}

	public List<SubjectRealisationNotification> getSubjectRealisationNotifications() {
		return subjectRealisationNotifications;
	}

	public void setSubjectRealisationNotifications(List<SubjectRealisationNotification> subjectRealisationNotifications) {
		this.subjectRealisationNotifications = subjectRealisationNotifications;
	}

	public Employee(String name, String lastName, EmployeeType employeeType, Address address, User user) {
		this.name = name;
		this.lastName = lastName;
		this.employeeType = employeeType;
		this.address = address;
		this.user = user;
	}

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}




}

