package rs.singi.dev.restapp.model.subject.examination;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.file.FSFile;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Klasa koja sluzi da veze dodatni materijal za odredjenu evaluaciju
 */
@Entity
public class SubjectExaminationMaterial extends AbstractEntity {

    @ManyToOne
    private SubjectExaminationDetails subjectExaminationDetails;

    @ManyToOne
    private FSFile fsFile;

    public SubjectExaminationMaterial() {
    }

    public SubjectExaminationDetails getSubjectExaminationDetails() {
        return subjectExaminationDetails;
    }

    public void setSubjectExaminationDetails(SubjectExaminationDetails subjectExaminationDetails) {
        this.subjectExaminationDetails = subjectExaminationDetails;
    }

    public FSFile getFsFile() {
        return fsFile;
    }

    public void setFsFile(FSFile fsFile) {
        this.fsFile = fsFile;
    }
}
