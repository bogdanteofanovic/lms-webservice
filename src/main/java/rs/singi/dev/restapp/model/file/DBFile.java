package rs.singi.dev.restapp.model.file;
import rs.singi.dev.restapp.model.employee.Employee;
import rs.singi.dev.restapp.model.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "dbFile")
public class DBFile extends AbstractFile {
    @Lob
    @Column(nullable = false, columnDefinition = "BLOB")
    private byte[] data;

    public DBFile(String fileName, String fileType, User user, LocalDateTime createdDate, LocalDateTime lastModifiedDate, byte[] data) {
        super(fileName, fileType, user, createdDate, lastModifiedDate);
        this.data = data;
    }

    public DBFile() {
        super();
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
