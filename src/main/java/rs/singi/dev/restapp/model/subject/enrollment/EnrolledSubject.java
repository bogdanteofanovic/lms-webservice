package rs.singi.dev.restapp.model.subject.enrollment;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.university.Student;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisation;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * Upisani predmet konkretnog studenta
 */
@Entity
public class EnrolledSubject extends AbstractEntity {


	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private SubjectRealisation subjectRealisation;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private EnrollmentType enrollmentType;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Student student;

	public SubjectRealisation getSubjectRealisation() {
		return subjectRealisation;
	}

	public void setSubjectRealisation(SubjectRealisation subjectRealisation) {
		this.subjectRealisation = subjectRealisation;
	}

	public EnrollmentType getEnrollmentType() {
		return enrollmentType;
	}

	public void setEnrollmentType(EnrollmentType enrollmentType) {
		this.enrollmentType = enrollmentType;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public EnrolledSubject(SubjectRealisation subjectRealisation,
			EnrollmentType enrollmentType, Student student) {
		this.subjectRealisation = subjectRealisation;
		this.enrollmentType = enrollmentType;
		this.student = student;
	}

	public EnrolledSubject() {
		super();
		// TODO Auto-generated constructor stub
	}


}
