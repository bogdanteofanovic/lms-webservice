package rs.singi.dev.restapp.model.university;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.address.Address;
import rs.singi.dev.restapp.model.employee.Employee;
import rs.singi.dev.restapp.model.university.faculty.Faculty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;


@Entity
public class University extends AbstractEntity {
	@Column(nullable = false)
	private String name;
	
	@OneToOne(optional = false)
	private Address address;

	@Column(nullable = false)
	private String phoneNumber;

	@JsonIgnore
	@OneToMany(mappedBy = "university")
	List<Faculty> faculties = new ArrayList<>();

	@Column(nullable = false)
	private String description;

	@OneToOne
	private Employee rector;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Faculty> getFaculties() {
		return faculties;
	}

	public void setFaculties(List<Faculty> faculties) {
		this.faculties = faculties;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Employee getRector() {
		return rector;
	}

	public void setRector(Employee rector) {
		this.rector = rector;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public University(String name, Address address, String phoneNumber, String description, Employee rector) {
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.description = description;
		this.rector = rector;
	}

	public University() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
