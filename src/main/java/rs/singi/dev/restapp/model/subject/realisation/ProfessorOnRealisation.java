package rs.singi.dev.restapp.model.subject.realisation;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.employee.Employee;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisation;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationType;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class ProfessorOnRealisation extends AbstractEntity {

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private SubjectRealisation subjectRealisation;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Employee professor;
	

	public SubjectRealisation getSubjectRealisation() {
		return subjectRealisation;
	}

	public void setSubjectRealisation(SubjectRealisation subjectRealisation) {
		this.subjectRealisation = subjectRealisation;
	}

	public Employee getEmployee() {
		return professor;
	}

	public void setEmployee(Employee employee) {
		this.professor = employee;
	}



	public ProfessorOnRealisation(SubjectRealisation subjectRealisation, Employee professor) {
		this.subjectRealisation = subjectRealisation;
		this.professor = professor;
	}

	public ProfessorOnRealisation() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
