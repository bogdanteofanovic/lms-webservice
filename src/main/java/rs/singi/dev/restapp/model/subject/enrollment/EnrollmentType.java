package rs.singi.dev.restapp.model.subject.enrollment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class EnrollmentType extends AbstractEntity {
    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "enrollmentType")
    private List<EnrolledSubject> enrolledSubjects = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EnrolledSubject> getEnrolledSubjects() {
        return enrolledSubjects;
    }

    public void setEnrolledSubjects(List<EnrolledSubject> enrolledSubjects) {
        this.enrolledSubjects = enrolledSubjects;
    }

    public EnrollmentType(String name) {
        this.name = name;
    }

    public EnrollmentType() {
    }
}
