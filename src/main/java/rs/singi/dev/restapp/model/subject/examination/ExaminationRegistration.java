package rs.singi.dev.restapp.model.subject.examination;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.university.Student;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

/**
 * Klasa u kojoj se vrsi prijava studenta na odredjenu evaluaciju
 */
@Entity
public class ExaminationRegistration extends AbstractEntity {

    @Column(nullable = false)
    private LocalDateTime dateOfRegistration;

    @ManyToOne(fetch = FetchType.LAZY)
    private ScheduleExamination scheduleExamination;

    @ManyToOne(fetch = FetchType.LAZY)
    private Student student;

    public LocalDateTime getDateOfRegistration() {
        return dateOfRegistration;
    }

    public void setDateOfRegistration(LocalDateTime dateOfRegistration) {
        this.dateOfRegistration = dateOfRegistration;
    }

    public ScheduleExamination getScheduleExamination() {
        return scheduleExamination;
    }

    public void setScheduleExamination(ScheduleExamination scheduleExamination) {
        this.scheduleExamination = scheduleExamination;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
