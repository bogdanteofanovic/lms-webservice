package rs.singi.dev.restapp.model.subject.realisation;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.file.FSFile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class SubjectRealisationMaterial extends AbstractEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private SubjectRealisation subjectRealisation;

    @ManyToOne(fetch = FetchType.LAZY)
    private FSFile fsFile;

    public SubjectRealisationMaterial() {
    }

    public SubjectRealisation getSubjectRealisation() {
        return subjectRealisation;
    }

    public void setSubjectRealisation(SubjectRealisation subjectRealisation) {
        this.subjectRealisation = subjectRealisation;
    }

    public FSFile getFsFile() {
        return fsFile;
    }

    public void setFsFile(FSFile fsFile) {
        this.fsFile = fsFile;
    }
}
