package rs.singi.dev.restapp.model.subject.realisation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationDetails;
import rs.singi.dev.restapp.model.university.AcademicYear;
import rs.singi.dev.restapp.model.subject.enrollment.EnrolledSubject;
import rs.singi.dev.restapp.model.subject.Subject;
import rs.singi.dev.restapp.model.subject.examination.SubjectExamination;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Realizacija odredjenog predmeta u akademskoj godini
 */
@Entity
public class SubjectRealisation extends AbstractEntity {

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private SubjectRealisationType subjectRealisationType;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private AcademicYear academicYear;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Subject subject;

	@JsonIgnore
	@OneToMany(mappedBy = "subjectRealisation")
	private List<EnrolledSubject> enrolledSubjects = new ArrayList<>();

	@JsonIgnore
	@OneToMany(mappedBy = "subjectRealisation")
	private List<SubjectExaminationDetails> subjectExaminations = new ArrayList<>();

	@JsonIgnore
	@OneToMany(mappedBy = "subjectRealisation")
	private List<ProfessorOnRealisation> professorOnRealisations = new ArrayList<>();

	@JsonIgnore
	@OneToMany(mappedBy = "subjectRealisation")
	private List<SubjectExaminationDetails> subjectExaminationDetails = new ArrayList<>();

	@JsonIgnore
	@OneToMany(mappedBy = "subjectRealisation")
	private List<ScheduleRealisation> scheduleRealisations = new ArrayList<>();

	@JsonIgnore
	@OneToMany(mappedBy = "subjectRealisation")
	private List<SubjectRealisationMaterial> subjectRealisationMaterials = new ArrayList<>();

	@Transient
	private int subjectRealisationMaterialsNum;

	@JsonIgnore
	@OneToMany(mappedBy = "subjectRealisation")
	private List<SubjectRealisationNotification> subjectRealisationNotifications = new ArrayList<>();

	public SubjectRealisationType getSubjectRealisationType() {
		return subjectRealisationType;
	}

	public void setSubjectRealisationType(SubjectRealisationType subjectRealisationType) {
		this.subjectRealisationType = subjectRealisationType;
	}

	public AcademicYear getAcademicYear() {
		return academicYear;
	}

	public void setAcademicYear(AcademicYear academicYear) {
		this.academicYear = academicYear;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public List<EnrolledSubject> getEnrolledSubjects() {
		return enrolledSubjects;
	}

	public void setEnrolledSubjects(List<EnrolledSubject> enrolledSubjects) {
		this.enrolledSubjects = enrolledSubjects;
	}

	public List<SubjectExaminationDetails> getSubjectExaminations() {
		return subjectExaminations;
	}

	public void setSubjectExaminations(List<SubjectExaminationDetails> subjectExaminations) {
		this.subjectExaminations = subjectExaminations;
	}

	public List<ProfessorOnRealisation> getProfessorOnRealisations() {
		return professorOnRealisations;
	}

	public void setProfessorOnRealisations(List<ProfessorOnRealisation> professorOnRealisations) {
		this.professorOnRealisations = professorOnRealisations;
	}

	public List<SubjectExaminationDetails> getSubjectExaminationDetails() {
		return subjectExaminationDetails;
	}

	public void setSubjectExaminationDetails(List<SubjectExaminationDetails> subjectExaminationDetails) {
		this.subjectExaminationDetails = subjectExaminationDetails;
	}

	public List<ScheduleRealisation> getScheduleRealisations() {
		return scheduleRealisations;
	}

	public void setScheduleRealisations(List<ScheduleRealisation> scheduleRealisations) {
		this.scheduleRealisations = scheduleRealisations;
	}

	public List<SubjectRealisationMaterial> getSubjectRealisationMaterials() {
		return subjectRealisationMaterials;
	}

	public void setSubjectRealisationMaterials(List<SubjectRealisationMaterial> subjectRealisationMaterials) {
		this.subjectRealisationMaterials = subjectRealisationMaterials;
	}

	public List<SubjectRealisationNotification> getSubjectRealisationNotifications() {
		return subjectRealisationNotifications;
	}

	public void setSubjectRealisationNotifications(List<SubjectRealisationNotification> subjectRealisationNotifications) {
		this.subjectRealisationNotifications = subjectRealisationNotifications;
	}

	public int getSubjectRealisationMaterialsNum() {
		return subjectRealisationMaterials.size();
	}

	public void setSubjectRealisationMaterialsNum(int subjectRealisationMaterialsNum) {
		this.subjectRealisationMaterialsNum = subjectRealisationMaterialsNum;
	}

	public SubjectRealisation(AcademicYear academicYear, Subject subject) {
		this.academicYear = academicYear;
		this.subject = subject;
	}

	public SubjectRealisation() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
