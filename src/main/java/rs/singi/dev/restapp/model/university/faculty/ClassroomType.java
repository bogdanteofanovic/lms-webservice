package rs.singi.dev.restapp.model.university.faculty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class ClassroomType extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "classroomType")
    private List<Classroom> classrooms;


    public ClassroomType(String name) {
        this.name = name;
    }

    public ClassroomType() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Classroom> getClassrooms() {
        return classrooms;
    }

    public void setClassrooms(List<Classroom> classrooms) {
        this.classrooms = classrooms;
    }
}
