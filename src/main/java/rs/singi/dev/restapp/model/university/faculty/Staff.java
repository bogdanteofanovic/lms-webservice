package rs.singi.dev.restapp.model.university.faculty;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.employee.Employee;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class Staff extends AbstractEntity {

    @ManyToOne(optional = false)
    private Employee employee;

    @ManyToOne
    private StudyProgram studyProgram;

    @ManyToOne(optional = false)
    private StaffType staffType;

    @Column(nullable = false)
    private Boolean active;

    @Column(nullable = false)
    private LocalDate employmentStart;

    @Column
    private LocalDate employmentEnd;

    public Staff() {
        this.active = true;
        this.employmentStart = LocalDate.now();
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public StudyProgram getStudyProgram() {
        return studyProgram;
    }

    public void setStudyProgram(StudyProgram studyProgram) {
        this.studyProgram = studyProgram;
    }

    public StaffType getStaffType() {
        return staffType;
    }

    public void setStaffType(StaffType staffType) {
        this.staffType = staffType;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public LocalDate getEmploymentStart() {
        return employmentStart;
    }

    public void setEmploymentStart(LocalDate employmentStart) {
        this.employmentStart = employmentStart;
    }

    public LocalDate getEmploymentEnd() {
        return employmentEnd;
    }

    public void setEmploymentEnd(LocalDate employmentEnd) {
        this.employmentEnd = employmentEnd;
    }
}
