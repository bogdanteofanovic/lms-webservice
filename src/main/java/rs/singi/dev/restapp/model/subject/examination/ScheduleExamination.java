package rs.singi.dev.restapp.model.subject.examination;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasa koja kreira rok za prijavu za odredjenu evaluaciju
 */

@Entity
public class ScheduleExamination extends AbstractEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private SubjectExaminationDetails subjectExaminationDetails;

    @Column(nullable = false)
    private LocalDateTime beginTime;

    @Column(nullable = false)
    private LocalDateTime endTime;

    @JsonIgnore
    @OneToMany(mappedBy = "scheduleExamination")
    private List<ExaminationRegistration> examinationRegistrations = new ArrayList<>();

    public ScheduleExamination() {
    }

    public SubjectExaminationDetails getSubjectExaminationDetails() {
        return subjectExaminationDetails;
    }

    public void setSubjectExaminationDetails(SubjectExaminationDetails subjectExaminationDetails) {
        this.subjectExaminationDetails = subjectExaminationDetails;
    }

    public LocalDateTime getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(LocalDateTime beginTime) {
        this.beginTime = beginTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public List<ExaminationRegistration> getExaminationRegistrations() {
        return examinationRegistrations;
    }

    public void setExaminationRegistrations(List<ExaminationRegistration> examinationRegistrations) {
        this.examinationRegistrations = examinationRegistrations;
    }
}
