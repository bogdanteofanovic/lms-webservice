package rs.singi.dev.restapp.model.employee;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class EmployeeType extends AbstractEntity {
    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "employeeType")
    private List<Employee> employees = new ArrayList<>();

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmployeeType(String name, List<Employee> employees) {
        this.name = name;
        this.employees = employees;
    }

    public EmployeeType() {
    }
}
