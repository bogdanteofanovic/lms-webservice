package rs.singi.dev.restapp.model.university;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.subject.Subject;
import rs.singi.dev.restapp.model.university.faculty.StudyProgram;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class YearOfStudy extends AbstractEntity {
	@Column(nullable = false)
	private String name;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private StudyProgram studyProgram;

	@JsonIgnore
	@OneToMany(mappedBy = "yearOfStudy")
	List<Subject> subjects = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setNaziv(String name) {
		this.name = name;
	}

	public StudyProgram getStudyProgram() {
		return studyProgram;
	}

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	public void setStudyProgram(StudyProgram studyProgram) {
		this.studyProgram = studyProgram;
	}

	public YearOfStudy(String name, StudyProgram studyProgram) {
		this.name = name;
		this.studyProgram = studyProgram;
	}

	public YearOfStudy() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
