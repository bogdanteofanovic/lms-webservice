package rs.singi.dev.restapp.model.file;
import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.employee.Employee;
import rs.singi.dev.restapp.model.subject.examination.SubjectExaminationMaterial;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisationMaterial;
import rs.singi.dev.restapp.model.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "fsFile")
public class FSFile extends AbstractFile {
    @Column(nullable = false)
    private String filePath;

    @JsonIgnore
    @OneToMany(mappedBy = "fsFile")
    private List<SubjectExaminationMaterial> subjectExaminationMaterials = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "fsFile")
    private List<SubjectRealisationMaterial> subjectRealisationMaterials = new ArrayList<>();

    public FSFile(String fileName, String fileType, User user, LocalDateTime createdDate, LocalDateTime lastModifiedDate, String filePath) {
        super(fileName, fileType, user, createdDate, lastModifiedDate);
        this.filePath = filePath;
    }

    public FSFile() {
        super();
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public List<SubjectExaminationMaterial> getSubjectExaminationMaterials() {
        return subjectExaminationMaterials;
    }

    public void setSubjectExaminationMaterials(List<SubjectExaminationMaterial> subjectExaminationMaterials) {
        this.subjectExaminationMaterials = subjectExaminationMaterials;
    }

    public List<SubjectRealisationMaterial> getSubjectRealisationMaterials() {
        return subjectRealisationMaterials;
    }

    public void setSubjectRealisationMaterials(List<SubjectRealisationMaterial> subjectRealisationMaterials) {
        this.subjectRealisationMaterials = subjectRealisationMaterials;
    }
}
