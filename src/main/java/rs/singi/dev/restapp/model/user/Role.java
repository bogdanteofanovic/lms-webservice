package rs.singi.dev.restapp.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.user.UserRole;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Role extends AbstractEntity {
    @Column(nullable = false, unique = true)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "role")
    private Set<UserRole> userRoles;

    public Role(String name, Set<UserRole> userRoles) {
        this.name = name;
        this.userRoles = userRoles;
    }

    public Role() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }
}

