package rs.singi.dev.restapp.model.file;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public class AbstractFile extends AbstractEntity {
    @Column(nullable = false, unique = true)
    private String fileName;

    @Column(nullable = false)
    private String fileType;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User user;

    @Column(nullable = false)
    private LocalDateTime createdDate;

    @Column(nullable = false)
    private LocalDateTime lastModifiedDate;

    @Transient
    private byte[] data;

    public AbstractFile(String fileName, String fileType, User user, LocalDateTime createdDate, LocalDateTime lastModifiedDate) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.user = user;
        this.createdDate = createdDate;
        this.lastModifiedDate = lastModifiedDate;
    }

    public AbstractFile() {
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
