package rs.singi.dev.restapp.model.subject.realisation;

import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.employee.Employee;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public class SubjectRealisationNotification extends AbstractEntity {

    @ManyToOne
    private SubjectRealisation subjectRealisation;

    @Column(nullable = false)
    private LocalDateTime publishDate;

    @Column(nullable = false)
    private String notification;

    @ManyToOne
    private Employee publisher;

    public SubjectRealisationNotification() {
    }

    public SubjectRealisation getSubjectRealisation() {
        return subjectRealisation;
    }

    public void setSubjectRealisation(SubjectRealisation subjectRealisation) {
        this.subjectRealisation = subjectRealisation;
    }

    public LocalDateTime getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDateTime publishDate) {
        this.publishDate = publishDate;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Employee getPublisher() {
        return publisher;
    }

    public void setPublisher(Employee publisher) {
        this.publisher = publisher;
    }
}
