package rs.singi.dev.restapp.model.address;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.employee.Employee;
import rs.singi.dev.restapp.model.university.Student;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Address extends AbstractEntity {
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String number;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private City city;

	@JsonIgnore
	@OneToMany(mappedBy = "address")
	List<Employee> employees = new ArrayList<>();

	@JsonIgnore
	@OneToMany(mappedBy = "address")
	List<Student> students = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public Address(String name, String number, City city) {
		this.name = name;
		this.number = number;
		this.city = city;
	}

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}
}
