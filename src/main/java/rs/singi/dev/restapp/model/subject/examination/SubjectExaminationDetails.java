package rs.singi.dev.restapp.model.subject.examination;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rs.singi.dev.restapp.model.AbstractEntity;
import rs.singi.dev.restapp.model.subject.realisation.SubjectRealisation;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasa u kojoj se kreira samo odrzavanje evaluacije i detalji oko nje, datum i tip
 */
@Entity
public class SubjectExaminationDetails extends AbstractEntity {

    @ManyToOne
    private SubjectExaminationType subjectExaminationType;

    @Column(nullable = false)
    private Boolean active;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SubjectRealisation subjectRealisation;

    @Column(nullable = false)
    private LocalDateTime dateAndTime;

    @JsonIgnore
    @OneToMany(mappedBy = "subjectExaminationDetails")
    private List<ScheduleExamination> scheduleExaminations = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "subjectExaminationDetails")
    private List<SubjectExaminationMaterial> subjectExaminationMaterials = new ArrayList<>();

    @Transient
    private int subjectExaminationMaterialsNum;

    public SubjectExaminationDetails() {
        super();
        this.active = true;
    }

    public SubjectExaminationType getSubjectExaminationType() {
        return subjectExaminationType;
    }

    public void setSubjectExaminationType(SubjectExaminationType subjectExaminationType) {
        this.subjectExaminationType = subjectExaminationType;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public SubjectRealisation getSubjectRealisation() {
        return subjectRealisation;
    }

    public void setSubjectRealisation(SubjectRealisation subjectRealisation) {
        this.subjectRealisation = subjectRealisation;
    }

    public LocalDateTime getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(LocalDateTime dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public List<ScheduleExamination> getScheduleExaminations() {
        return scheduleExaminations;
    }

    public void setScheduleExaminations(List<ScheduleExamination> scheduleExaminations) {
        this.scheduleExaminations = scheduleExaminations;
    }

    public List<SubjectExaminationMaterial> getSubjectExaminationMaterials() {
        return subjectExaminationMaterials;
    }

    public void setSubjectExaminationMaterials(List<SubjectExaminationMaterial> subjectExaminationMaterials) {
        this.subjectExaminationMaterials = subjectExaminationMaterials;
    }

    public int getSubjectExaminationMaterialsNum() {
        return subjectExaminationMaterials.size();
    }

    public void setSubjectExaminationMaterialsNum(int subjectExaminationMaterialsNum) {
        this.subjectExaminationMaterialsNum = subjectExaminationMaterialsNum;
    }
}
