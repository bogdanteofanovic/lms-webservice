package rs.singi.dev.restapp.socket.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.*;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import rs.singi.dev.restapp.socket.handler.CustomWebSocketHandler;

import java.util.logging.SocketHandler;


@Configuration
@EnableWebSocket
public class SocketConfig implements WebSocketConfigurer {


    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
        webSocketHandlerRegistry.addHandler(new CustomWebSocketHandler(), "/gridEvents").setAllowedOrigins("*");
    }
    


}
