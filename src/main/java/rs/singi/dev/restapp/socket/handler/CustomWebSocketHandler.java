package rs.singi.dev.restapp.socket.handler;

import com.google.gson.Gson;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
public class CustomWebSocketHandler extends TextWebSocketHandler {



    List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();



    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
            throws InterruptedException, IOException {
            System.out.println(message.getPayload());
        for(WebSocketSession webSocketSession : sessions) {
            if(webSocketSession.isOpen()){
                webSocketSession.sendMessage(new TextMessage(message.getPayload()));
            }
        }
    }



    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        //the messages will be broadcasted to all users.
        sessions.add(session);
    }

}
